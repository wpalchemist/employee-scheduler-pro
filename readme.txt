=== Employee Scheduler Pro ===
Contributors: gwendydd
Tags: employees, schedule, timesheet
Requires at least: 3.8
Tested up to: 4.5.3
Stable tag: 1.0


== Description ==

Pro features:
* bulk create shifts
* bulk delete and bulk edit shifts
* payroll report
* scheduled/worked report
* filter expenses
* filter shifts
* dashboard widgets show you currenty clocked-in employees and recent employee notes
* manager user role


== Changelog ==
= 1.4.0 =
* fixed bug in overtime calculation on payroll report
* fixed bug in pay calculation on payroll report
* improved availability form
* ensured that shifts created with the bulk shift creator all have unique slugs
* compatibility with Employee Scheduler On Demand
* compatibility with Employee Scheduler Text Notifications
* added "location" to bulk shift creator and bulk shift updater
* added currency code to payroll report
* added filter so that you can change "employee" to "volunteer" or anything else
* added Manager user role - managers have access to Employee Scheduler functionality in the dashboard, can be connected to employees, receive admin notificatons related to their employees
* added "Schedule Conflicts" to employee_profile shortcode so employees can enter the times they are unavailable

= 1.3.2 =
* make sure "take shift" buttons don't show up when viewing schedule in dashboard
* fixed bugs in overtime calculation on payroll report
* fixed bug in bulk shift updater
* when viewing a single job, also display all unassigned shifts for that job

= 1.3.1 =
* fixed typo that sometimes broke employee schedule conflict checking
* made payroll report work even if you have not selected scheduled or actual on settings page
* fixed bug that prevented license from activating properly

= 1.3 =
* fixed bug that caused error messages when bulk creating shifts without employees
* fixed bug that prevented date and time from saving on bulk shifts without employees
* change h2 to h1 on admin screens to conform to new accessibility standards
* security improvements
* employees can now take unassigned shifts
* employees can drop shifts
* payroll report is now downloadable as a .xlsx
* added [unassigned_shifts] shortcode to display unassigned shifts and let employees pick up unassigned shifts
* admins can receive notifications when employees pick up and drop shifts
* employees can receive notifications when they pick up shifts

= 1.2 =
* fixed bug in filter so that you can filter by employee and job simultaneously
* payroll report totals rounded to 2 decimal places
* fixed bug that made "payroll information" header show on employee_profile shortcode
* "clocked in now" dashboard widget orders shifts by clock-in time
* added schedule conflict checking

= 1.1 =
* made employee notifications on bulk shift creator more reliable

= 1.0 = 
* Initial Release