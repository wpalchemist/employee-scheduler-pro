<?php
/*
Plugin Name: Employee Scheduler Pro
Plugin URI: http://employee-scheduler.co
Description: Adds more features to the Employee Scheduler plugin
Version: 1.4.0
Author: Morgan Kay
Author URI: http://ran.ge
Text Domain: wpaesp
*/

/*  Copyright 2015 Morgan Kay (email : morgan@ran.ge)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// REQUIRE EMPLOYEE SCHEDULER                                            
// ------------------------------------------------------------------------


function wpaesp_require_employee_scheduler() {
	$active = get_option('active_plugins');
    if ( is_admin() && current_user_can( 'activate_plugins' ) && !in_array( 'employee-scheduler/employee-scheduler.php', $active ) ) {
        add_action( 'admin_notices', 'wpaesp_need_employee_scheduler_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}
add_action( 'admin_init', 'wpaesp_require_employee_scheduler' );

function wpaesp_need_employee_scheduler_notice(){
    ?><div class="error"><p><?php _e( 'Sorry, but Employee Scheduler Pro requires the <a href="https://wordpress.org/plugins/employee-scheduler/" target="_blank">Employee Scheduler</a> to be installed and active.', 'wpaesp' ); ?></p></div><?php
}

// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
register_activation_hook(__FILE__, 'wpaesp_add_defaults');
register_uninstall_hook(__FILE__, 'wpaesp_delete_plugin_options');
register_activation_hook( __FILE__, 'wpaesp_create_manager_user_role' );
add_action('admin_init', 'wpaesp_options_init' );
add_action('admin_menu', 'wpaesp_add_meu_items' );

// Require bulk shift creator
require_once( plugin_dir_path( __FILE__ ) . 'bulk-shift-creator.php' );
// Require reports
require_once( plugin_dir_path( __FILE__ ) . 'reports.php' );
// Require filter
require_once( plugin_dir_path( __FILE__ ) . 'filter.php' );
// Require dashboard widgets
require_once( plugin_dir_path( __FILE__ ) . 'dashboard-widgets.php' );
// Require manager
require_once( plugin_dir_path( __FILE__ ) . 'manager.php' );
$options = get_option( 'wpaesm_options' );
if( isset( $options['self_assign'] ) && '1' == $options['self_assign'] ) {
	// Require pick-up - this is everything that allows employees to pick up unassigned shifts
	require_once( plugin_dir_path( __FILE__ ) . 'pick-up.php' );
}
if( isset( $options['allow_drop'] ) && '1' == $options['allow_drop'] ) {
	// Require drop - this is everything that allows employees to drop shifts
	require_once( plugin_dir_path( __FILE__ ) . 'drop.php' );
}

// add pages to menu 
function wpaesp_add_meu_items() {
	add_menu_page( 
		'Employee Scheduler', 
		'Employee Scheduler', 
		'manage_options', 
		'/employee-scheduler/options.php', 
		'wpaesm_render_options', 
		'dashicons-admin-generic', 
		87.2317 
	);
	add_submenu_page( '/employee-scheduler/options.php', 'Payroll Report', 'Payroll Report', 'manage_options', 'payroll-report', 'wpaesp_payroll_report' );
	add_submenu_page( '/employee-scheduler/options.php', 'Scheduled/Worked', 'Scheduled/Worked', 'manage_options', 'scheduled-worked', 'wpaesp_scheduled_worked_report' );
	add_submenu_page( 'edit.php?post_type=shift', 'Bulk Add Shifts', 'Bulk Add Shifts', 'manage_options', 'add-repeating-shifts', 'wpaesp_bulk_shift_form' );
}


// Initialize language so it can be translated
function wpaesp_language_init() {
  load_plugin_textdomain( 'wpaesp', false, dirname(plugin_basename(__FILE__)) . 'languages' );
}
add_action('init', 'wpaesp_language_init');

define( 'WPAESP_PATH', plugin_dir_path(__FILE__) );

// Add stylesheets and scripts
function wpaesp_admin_enqueue_scripts($hook)
{
	if( $hook == 'post-new.php' || $hook == 'post.php' || $hook == 'edit.php' ) {
		global $post;
		if( is_object( $post ) ) {
			if ( 'shift' === $post->post_type ) { 
				$options = get_option('wpaesp_options');
				wp_enqueue_script( 'employee-scheduler-pro', plugin_dir_url(__FILE__) . 'assets/employee-scheduler-pro.js', array( 'jquery' ) );
            	wp_localize_script( 'employee-scheduler-pro', 'wpaesp_shiftajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'avoid_conflicts' => $options['avoid_conflicts'] ) ); 
			}
		}
	}
	if ( 'user-edit.php' == $hook || 'profile.php' == $hook ) {
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-scheduler/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'employee-scheduler-pro', plugin_dir_url(__FILE__) . 'assets/employee-scheduler-pro.js', array( 'jquery' ) );
	}
	global $post;
	if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'employee_profile') ) {
		wp_enqueue_script( 'employee-scheduler-pro', plugin_dir_url(__FILE__) . 'assets/employee-scheduler-pro.js', array( 'jquery' ) );
	}
}
add_action( 'admin_enqueue_scripts', 'wpaesp_admin_enqueue_scripts' );

function wpaesp_public_enqueue_scripts() {

	global $post;
	if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'employee_profile') ) {
		wp_enqueue_script( 'employee-scheduler-pro', plugin_dir_url(__FILE__) . 'assets/employee-scheduler-pro.js', array( 'jquery' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'wpaesp_public_enqueue_scripts' );


// ------------------------------------------------------------------------
// LICENSES AND UPDATES
// ------------------------------------------------------------------------

// Automatic updates
define( 'WPAESP_STORE_URL', 'https://employee-scheduler.co' );
define( 'WPAESP_ITEM_NAME', 'Employee Scheduler Pro' );
if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
    // load our custom updater if it doesn't already exist 
    include( plugin_dir_path( __FILE__ ) . 'EDD_SL_Plugin_Updater.php' );
}

function wpaesp_automatic_updater() {

    // retrieve our license key from the DB
    $options = get_option( 'wpaesp_options' );
    if( isset( $options['pro_license'] ) ) {
        $license_key = trim( $options['pro_license'] );

        // setup the updater
        $edd_updater = new EDD_SL_Plugin_Updater( WPAESP_STORE_URL, __FILE__, array(
                'version'   => '1.4.0',               // current version number
                'license'   => $license_key,        // license key (used get_option above to retrieve from DB)
                'item_name' => WPAESP_ITEM_NAME,    // name of this plugin
                'author'    => 'Morgan Kay'  // author of this plugin
            )
        );
    }

}
add_action( 'admin_init', 'wpaesp_automatic_updater', 0 );

// activate license key
function wpaesp_activate_license() {

    if( isset( $_POST['wpaesp_license_activate'] ) ) {

        if( ! check_admin_referer( 'wpaesp_license_activation_nonce', 'wpaesp_license_activation_nonce' ) )   
            return;

        // retrieve the license from the database
        $options = get_option( 'wpaesm_options' );
        $license = trim( $options['pro_license'] );

        // data to send in our API request
        $api_params = array( 
            'edd_action'=> 'activate_license', 
            'license'   => $license, 
            'item_name' => urlencode( WPAESP_ITEM_NAME ), // the name of our product in EDD
            'url'       => home_url()
        );
        
        // Call the custom API.
        $response = wp_remote_get( add_query_arg( $api_params, WPAESP_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

        // make sure the response came back okay
        if ( is_wp_error( $response ) )
            return false;

        // decode the license data
        $license_data = json_decode( wp_remote_retrieve_body( $response ) );

        update_option('wpaesp_license', $license_data->license);

    }
}
add_action('admin_init', 'wpaesp_activate_license');

function wpaesp_deactivate_license() {

    // listen for our activate button to be clicked
    if( isset( $_POST['wpaesp_license_deactivate'] ) ) {

        // run a quick security check 
        if( ! check_admin_referer( 'wpaesp_license_activation_nonce', 'wpaesp_license_activation_nonce' ) )   
            return; // get out if we didn't click the Activate button

        // retrieve the license from the database
        $options = get_option( 'wpaesm_options' );
        $license = trim( $options['pro_license'] );
            

        // data to send in our API request
        $api_params = array( 
            'edd_action'=> 'deactivate_license', 
            'license'   => $license, 
            'item_name' => urlencode( WPAESP_ITEM_NAME ), // the name of our product in EDD
            'url'       => home_url()
        );
        
        // Call the custom API.
        $response = wp_remote_get( add_query_arg( $api_params, WPAESP_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

        // make sure the response came back okay
        if ( is_wp_error( $response ) )
            return false;

        // decode the license data
        $license_data = json_decode( wp_remote_retrieve_body( $response ) );
        
        if( $license_data->license == 'deactivated' ) {
            update_option('wpaesp_license', $license_data->license);
        }

    }
}
add_action('admin_init', 'wpaesp_deactivate_license');

// ------------------------------------------------------------------------
// DELETE ALL SHIFTS IN A SERIES 
// largely borrowed from https://wordpress.org/plugins/completely-delete/
// ------------------------------------------------------------------------

add_action( 'plugins_loaded', 'wpaesp_completely_delete' );
add_filter( 'page_row_actions', 'wpaesp_row_actions', 10, 2 );


function wpaesp_get_action_url( $post_id ) {
	return wp_nonce_url( add_query_arg( array( 'action' => 'wpaesp_completely_delete', 'post' => $post_id ), admin_url( 'admin.php' ) ), 'wpaesp_delete_series', 'wpaesp_delete_nonce' );
}

function wpaesp_row_actions( $actions, $post ) {
	if ( 'trash' != $post->post_status && $post->post_type == 'shift' ) {
		$message = __( 'Are you sure you want to delete all of these shifts?', 'wpaesp' );
		$url = wpaesp_get_action_url( $post->ID );
		$actions['wpaesp'] = '<a class="wpaesp delete" href="#" onclick="if(confirm(\'' . $message . '\')) { window.location=\'' . $url . '\'; }">' . __( 'Delete All Shifts In Series', 'wpaesp' ) . '</a>';
	}

	return $actions;
}

function wpaesp_completely_delete() {
	if (!isset($_GET['wpaesp_delete_nonce']) || !wp_verify_nonce($_GET['wpaesp_delete_nonce'], 'wpaesp_delete_series')) {
		// do nothing
	} else {
		if(isset($_REQUEST['post'])) {
			$post_id = $_REQUEST['post'];
			$poststodelete = array();
			$i= 0;
			// get the children of this post, if there are any
			$children = get_posts('post_type=shift&posts_per_page=-1&post_parent=' . $post_id);
			foreach($children as $child) {
				$poststodelete[] = $child->ID;
				$i++;
			}

			// get the parent of this post, if there is one
			$parent = wp_get_post_parent_id( $post_id );
			if(isset($parent) && $parent != '') {
				$poststodelete[] = $parent;
				$i++;
				// get the other children of this post's parent
				$siblings = get_posts('post_type=shift&posts_per_page=-1&post_parent=' . $parent);
				foreach($siblings as $sibling) {
					$poststodelete[] = $sibling->ID;
					$i++;
				}
			}

			// delete all of the children and parents
			foreach($poststodelete as $postid) {
				wp_trash_post($postid);
			}
			wp_trash_post($post_id);  // delete the original post
			wp_redirect( admin_url( '/edit.php?post_type=shift' ) );
		}

	}

}

// ------------------------------------------------------------------------
// APPLY CHANGES TO SUBSEQUENT SHIFTS IN SERIES 
// ------------------------------------------------------------------------

function wpaesp_apply_changes_to_series() {
	if( !isset( $_POST['shift'] ) ) {
		$results = __( 'Error.  No shifts were updated.', 'wpaesp' );
		die( $results );
	}

	$post_id = $_POST['shift'];

	global $shift_publish_metabox; // get metabox data
	$meta = $shift_publish_metabox->the_meta($post_id); 

	// gather all of this shift's information
	$title = get_the_title( $post_id );
	$content = get_post_field( 'post_content', $post_id );
	global $shift_metabox;
	$postmeta = $shift_metabox->the_meta( $post_id );
	$type = wp_get_post_terms( $post_id, 'shift_type', array("fields" => "ids") );
	$status = wp_get_post_terms( $post_id, 'shift_status', array("fields" => "ids") );
	$location = wp_get_post_terms( $post_id, 'location', array("fields" => "ids") );
	$employees = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $post_id
	) );
	foreach( $employees as $employee ) {
		$employeeid = $employee->ID;
	}
	$jobs = get_posts( array(
	  'connected_type' => 'shifts_to_jobs',
	  'connected_items' => $post_id,
	  'nopaging' => true,
	  'suppress_filters' => false
	) );
	foreach( $jobs as $job ) {
		$jobid = $job->ID;
	}

	// find all of the subsequent shifts
	$parent = wp_get_post_parent_id( $post_id );
	if( 0 === $parent ) {
		// this doesn't have a parent, so we need to get the children
		$args = array(
			'post_type' => 'shift',
			'posts_per_page' => '-1',
			'post_parent' => $post_id,
			'meta_key' => '_wpaesm_date',
			'meta_value' => $postmeta['date'],
			'meta_compare' => '>',
		);
	} else {
		// this is a child post, so we need to get the siblings
		$args = array(
			'post_type' => 'shift',
			'posts_per_page' => '-1',
			'post_parent' => $parent,
			'meta_key' => '_wpaesm_date',
			'meta_value' => $postmeta['date'],
			'meta_compare' => '>',
		);
	}
	
	$subsequent_shifts = new WP_Query( $args );

	$i = 0;
	$idlist = '';
	
	// update all subsequent shifts
	if ( $subsequent_shifts->have_posts() ) {
		while ( $subsequent_shifts->have_posts() ) : $subsequent_shifts->the_post();
			$id = get_the_id(); 
			// update basic post information
			$update = array(
				'ID' => $id,
				'post_title' => $title,
				'post_content' => $content,
				);
			wp_update_post( $update );
			// update shift meta: start time, end time
		    update_post_meta( $id, '_wpaesp_starttime', $postmeta['starttime'] );
		    update_post_meta( $id, '_wpaesp_endtime', $postmeta['endtime'] );

			// update shift type and status
			wp_set_post_terms( $id, $type, 'shift_type' );
			wp_set_post_terms( $id, $status, 'shift_status' );
			wp_set_post_terms( $id, $location, 'location' );


			// update connected users
			$oldemployees = get_users( array(
				'connected_type' => 'shifts_to_employees',
				'connected_items' => $id
			) );
			if( !empty( $oldemployees ) ) {
				foreach( $oldemployees as $oldemployee ) {
					$oldemployeeid = $oldemployee->ID;
				}
				p2p_type( 'shifts_to_employees' )->disconnect( $id, $oldemployeeid );
				p2p_type( 'shifts_to_employees' )->connect( $id, $employeeid, array(
				    'date' => current_time('mysql')
				) );
			}

			// update connected jobs
			$oldjobs = get_posts( array(
			  'connected_type' => 'shifts_to_jobs',
			  'connected_items' => $id,
			  'nopaging' => true,
			  'suppress_filters' => false
			) );
			foreach( $oldjobs as $oldjob ) {
				$oldjobid = $oldjob->ID;
			}
			p2p_type( 'shifts_to_jobs' )->disconnect( $id, $oldjobid );
			p2p_type( 'shifts_to_jobs' )->connect( $id, $jobid, array(
			    'date' => current_time('mysql')
			) );

			$i++;
		endwhile;
	} else {
		$results = __( 'No subsequent shifts were found.', 'wpaesp' );
		die( $results );
	}
	
	// Reset Post Data
	wp_reset_postdata();

	$results = $i;
	$results .= __( ' shifts were updated.', 'wpaesp' );

	die( $results );		
}
add_action( 'wp_ajax_wpaesp_apply_changes_to_series', 'wpaesp_apply_changes_to_series' );


// ------------------------------------------------------------------------
// CREATE SHIFT HISTORY METABOX
// ------------------------------------------------------------------------

if( !class_exists( 'WPAlchemy_MetaBox' ) ) {
	include_once 'assets/MetaBox.php';
}

$options = get_option( 'wpaesm_options' ); 
if( ( isset( $options['self_assign'] ) && '1' == $options['self_assign'] ) || ( isset( $options['allow_drop'] ) && '1' == $options['allow_drop'] ) ) {
	// Create metabox to display pick-up/drop history
	$shift_history = new WPAlchemy_MetaBox(array
	(
	    'id' => 'shift_history',
	    'title' => __( 'Shift Assignment History', 'wpaesp' ),
	    'types' => array('shift'),
	    'template' => plugin_dir_path(__FILE__) . '/shift-history.php',
	    'mode' => WPALCHEMY_MODE_EXTRACT,
	    'context' => 'side',
	    'priority' => 'low',
	    'prefix' => '_wpaesp_'
	));
}

// ------------------------------------------------------------------------
// CREATE EMPLOYEE USER FIELDS
// ------------------------------------------------------------------------

add_action( 'show_user_profile', 'wpaesp_employee_profile_fields' );
add_action( 'edit_user_profile', 'wpaesp_employee_profile_fields' );

function wpaesp_employee_profile_fields( $user ) {  ?>
	<?php if( is_admin() ) { ?>

		<?php if( !( wpaesm_check_user_role( 'es_manager' ) && $user->ID == get_current_user_id() ) ) { ?>
			<h3><?php _e("Payroll Information", "wpaesp"); ?></h3>

			<table class="form-table">
				<tr>
					<th><label for="wage"><?php _e( 'Pay Rate', 'wpaesp' ); ?></label></th>
					<td>
						<input type="text" name="wage" id="wage" value="<?php echo esc_attr( get_the_author_meta( 'wage', $user->ID ) ); ?>" class="regular-text" /><br />
						<span class="description"><?php _e( 'Enter the hourly pay rate, with no currency sign.  Example: 15', 'wpaesp' ); ?></span>
					</td>
				</tr>
				<tr>
					<th><label for="deduction"><?php _e( 'Deductions', 'wpaesp' ); ?></label></th>
					<td>
						<span class="description"><?php _e( 'For each deduction, enter the amount to be deducted from each paycheck, with no currency sign.  Example: 15', 'wpaesp' ); ?></span><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Health Insurance for Employee: ', 'wpaesp' ); ?></label><input type="text" name="deductions_health_self" id="deductions_health_self" value="<?php echo esc_attr( get_the_author_meta( 'deductions_health_self', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Health Insurance for Family: ', 'wpaesp' ); ?></label><input type="text" name="deductions_health_family" id="deductions_health_family" value="<?php echo esc_attr( get_the_author_meta( 'deductions_health_family', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Dental Insurance for Employee: ', 'wpaesp' ); ?></label><input type="text" name="deductions_dental_self" id="deductions_dental_self" value="<?php echo esc_attr( get_the_author_meta( 'deductions_dental_self', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Dental Insurance for Family: ', 'wpaesp' ); ?></label><input type="text" name="deductions_dental_family" id="deductions_dental_family" value="<?php echo esc_attr( get_the_author_meta( 'deductions_dental_family', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Wage Garnishment: ', 'wpaesp' ); ?></label><input type="text" name="deductions_garnish" id="deductions_garnish" value="<?php echo esc_attr( get_the_author_meta( 'deductions_garnish', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Extra Withholding: ', 'wpaesp' ); ?></label><input type="text" name="deductions_withhold" id="deductions_withhold" value="<?php echo esc_attr( get_the_author_meta( 'deductions_withhold', $user->ID ) ); ?>" class="small-text" /><br />
						<label style="display:inline-block;width:16em"><?php _e( 'Other: ', 'wpaesp' ); ?></label><input type="text" name="deductions_other" id="deductions_other" value="<?php echo esc_attr( get_the_author_meta( 'deductions_other', $user->ID ) ); ?>" class="small-text" /><br />
					</td>
				</tr>
				<tr>
					<th><label for="hours"><?php _e( 'Regular Hours Per Week', 'wpaesp' ); ?></label></th>
					<td>
						<input type="text" name="hours" id="hours" value="<?php echo esc_attr( get_the_author_meta( 'hours', $user->ID ) ); ?>" class="regular-text" /><br />
						<span class="description"><?php _e( 'Enter the number of regular hours the employee works per week.  If the employee works more hours, it will be considered overtime.  You can enter default regular hours on the Employee Scheduler settings page.  Example: 40', 'wpaesp' ); ?></span>
					</td>
				</tr>
			</table>
		<?php } ?>
	<?php } ?>
		<h3><?php _e("Schedule Conflicts", "wpaesp"); ?></h3>
		<p><?php _e( 'Record the times when you are regularly unavailable to work', 'employee-scheduler' ); ?></p>
		<table class="form-table">
			<tr>
				<th><label for="unavailable"><?php _e("Unavailable", "wpaesp"); ?></label></th>

				<td>
					<script type="text/template" id="date-time-template">
						<div class="repeating">
							<p class="third" style="display: inline-block;">
								<label><?php _e("Day", "wpaesp"); ?></label><br />
								<select name="unavailable[0][day]" id="unavailable[0][day]">
									<option value=""></option>
									<option value="sunday"><?php _e( 'Sunday', 'wpaesp' ); ?></option>
									<option value="monday"><?php _e( 'Monday', 'wpaesp' ); ?></option>
									<option value="tuesday"><?php _e( 'Tuesday', 'wpaesp' ); ?></option>
									<option value="wednesday"><?php _e( 'Wednesday', 'wpaesp' ); ?></option>
									<option value="thursday"><?php _e( 'Thursday', 'wpaesp' ); ?></option>
									<option value="friday"><?php _e( 'Friday', 'wpaesp' ); ?></option>
									<option value="saturday"><?php _e( 'Saturday', 'wpaesp' ); ?></option>
								</select>
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "From", "wpaesp"); ?></label><br />
								<input type="text" name="unavailable[0][start]" id="unavailable[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable_start[0][start]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "To", "wpaesp" ); ?></label><br />
								<input type="text" name="unavailable[0][end]" id="unavailable[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable[0][end]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<a href="#" class="remove"><?php _e( 'Remove', 'wpaesp'); ?></a>
						</div>
					</script>
					<?php $unavailable = get_the_author_meta( 'unavailable', $user->ID );
					if( is_array( $unavailable ) ) {
						$i = 0;
						foreach( $unavailable as $unavailability ) { ?>
							<div class="repeating">
								<p class="third" style="display: inline-block;">
									<label><?php _e("Day", "wpaesp"); ?></label><br />
									<select name="unavailable[<?php echo $i; ?>][day]" id="unavailable[<?php echo $i; ?>][day]">
										<option value=""></option>
										<option value="sunday" <?php selected( $unavailability['day'], 'sunday' ); ?>><?php _e( 'Sunday', 'wpaesp' ); ?></option>
										<option value="monday" <?php selected( $unavailability['day'], 'monday' ); ?>><?php _e( 'Monday', 'wpaesp' ); ?></option>
										<option value="tuesday" <?php selected( $unavailability['day'], 'tuesday' ); ?>><?php _e( 'Tuesday', 'wpaesp' ); ?></option>
										<option value="wednesday" <?php selected( $unavailability['day'], 'wednesday' ); ?>><?php _e( 'Wednesday', 'wpaesp' ); ?></option>
										<option value="thursday" <?php selected( $unavailability['day'], 'thursday' ); ?>><?php _e( 'Thursday', 'wpaesp' ); ?></option>
										<option value="friday" <?php selected( $unavailability['day'], 'friday' ); ?>><?php _e( 'Friday', 'wpaesp' ); ?></option>
										<option value="saturday" <?php selected( $unavailability['day'], 'saturday' ); ?>><?php _e( 'Saturday', 'wpaesp' ); ?></option>
									</select>
								</p>
								<p class="third" style="display: inline-block;">
									<label><?php _e( "From", "wpaesp"); ?></label><br />
									<input type="text" name="unavailable[<?php echo $i; ?>][start]" id="unavailable[<?php echo $i; ?>][start]" class="starttime" value="<?php echo $unavailability['start']; ?>" class="short-text" />
								</p>
								<p class="third" style="display: inline-block;">
									<label><?php _e( "To", "wpaesp" ); ?></label><br />
									<input type="text" name="unavailable[<?php echo $i; ?>][end]" id="unavailable[<?php echo $i; ?>][end]" class="endtime" value="<?php echo $unavailability['end']; ?>" class="short-text" />
								</p>
							    <a href="#" class="remove"><?php _e( 'Remove', 'wpaesp'); ?></a>
							</div>
						<?php $i++;
						}
					}  else { ?>
						<div class="repeating">
							<p class="third" style="display: inline-block;">
								<label><?php _e("Day", "wpaesp"); ?></label><br />
								<select name="unavailable[0][day]" id="unavailable[0][day]">
									<option value=""></option>
									<option value="sunday"><?php _e( 'Sunday', 'wpaesp' ); ?></option>
									<option value="monday"><?php _e( 'Monday', 'wpaesp' ); ?></option>
									<option value="tuesday"><?php _e( 'Tuesday', 'wpaesp' ); ?></option>
									<option value="wednesday"><?php _e( 'Wednesday', 'wpaesp' ); ?></option>
									<option value="thursday"><?php _e( 'Thursday', 'wpaesp' ); ?></option>
									<option value="friday"><?php _e( 'Friday', 'wpaesp' ); ?></option>
									<option value="saturday"><?php _e( 'Saturday', 'wpaesp' ); ?></option>
								</select>
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "From", "wpaesp"); ?></label><br />
								<input type="text" name="unavailable[0][start]" id="unavailable[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable_start[0][start]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "To", "wpaesp" ); ?></label><br />
								<input type="text" name="unavailable[0][end]" id="unavailable[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'unavailable[0][end]', $user->ID ) ); ?>" class="short-text" />
							</p>
							<a href="#" class="remove"><?php _e( 'Remove', 'wpaesp'); ?></a>
						</div>
					<?php } ?>
					<p><a href="#" class="repeat"><?php _e('Add Another', 'wpaesp'); ?></a></p>
					<span class="description" style="display:block;clear:both"><?php _e('Enter the days and times when employee is regularly unavailable.  This will be used to look for scheduling conflicts.', 'wpaesp' ); ?></span>
				</td>
			</tr>
		</table>
<?php }

add_action( 'personal_options_update', 'wpaesp_save_employee_profile_fields' );
add_action( 'edit_user_profile_update', 'wpaesp_save_employee_profile_fields' );

function wpaesp_save_employee_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	update_user_meta( $user_id, 'wage', floatval( $_POST['wage'] ) );
	update_user_meta( $user_id, 'deductions_health_self', floatval( $_POST['deductions_health_self'] ) );
	update_user_meta( $user_id, 'deductions_health_family', floatval( $_POST['deductions_health_family'] ) );
	update_user_meta( $user_id, 'deductions_dental_self', floatval( $_POST['deductions_dental_self'] ) );
	update_user_meta( $user_id, 'deductions_dental_family', floatval( $_POST['deductions_dental_family'] ) );
	update_user_meta( $user_id, 'deductions_garnish', floatval( $_POST['deductions_garnish'] ) );
	update_user_meta( $user_id, 'deductions_withhold', floatval( $_POST['deductions_withhold'] ) );
	update_user_meta( $user_id, 'deductions_other', floatval( $_POST['deductions_other'] ) );
	update_user_meta( $user_id, 'hours', floatval( $_POST['hours'] ) );
	// delete unavailability details before we save them so that removed values get removed
	delete_user_meta( $user_id, 'unavailable' );
	update_user_meta( $user_id, 'unavailable', $_POST['unavailable'] );
}



function wpaesp_pro_instructions() { ?>
	<p><?php _e('To bulk create shifts:', 'wpaesp'); ?>
			<ul>
				<li>
					<?php _e('Go to <a href="' . admin_url( 'edit.php?post_type=shift&page=add-repeating-shifts' ) . '">Shifts --> Bulk Add Shifts</a>.', 'wpaesp'); ?>
				</li>
				<li>
					<?php _e('Enter the appropriate information.', 'wpaesp'); ?>
				</li>
				<li>
					<?php _e('Click "Create Shifts."', 'wpaesp'); ?>
				</li>
			</ul>
		</p>

		<h3><?php _e('Payroll Reports', 'wpaesp'); ?></h3>

		<p><?php _e('To generate a payroll report, go to <a href="' . admin_url('page=payroll-report') . '">Employee Schedule Manager --> Payroll Report</a>.  Select the date range for the report, and click "Generate Report".', 'wpaesp'); ?></p>

<?php }
add_action( 'wpaesp_pro_instructions', 'wpaesp_pro_instructions ');

function wpaesp_display_support_sidebar() { ?>
	<h3><?php _e( 'Support', 'wpaesp' ); ?></h3>
	<p><?php _e( 'See our forums for fast, personal support', 'wpaesp' ); ?></p>
	<p><a href="https://employee-scheduler.co/forums/forum/employee-scheduler-pro-support/" target="_blank" class="button button-primary">
		<?php _e( 'Contact', 'wpaesp' ); ?>
	</a></p>
<?php }

add_action( 'init' , 'wpaesp_swap_ads' , 15 );
function wpaesp_swap_ads() {
	remove_action( 'wpaesm_options_sidebar', 'wpaesm_display_options_sidebar', 10 );
	add_action( 'wpaesm_options_sidebar', 'wpaesp_display_support_sidebar', 10 );
}


// ------------------------------------------------------------------------
// CHECK FOR SCHEDULING CONFLICTS
// ------------------------------------------------------------------------

// ajax function to check for schedule conflicts when you publish a post
function wpaesp_check_for_schedule_conflicts_before_publish() {

	if( !isset( $_POST['employee'] ) || '' == $_POST['employee'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected an employee.  Save this shift anyway?', 'wpaesp' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['day'] ) || '' == $_POST['day'] || '____-__-__' == $_POST['day'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected a date.  Save this shift anyway?', 'wpaesp' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['start'] ) || '' == $_POST['start'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected a start time.  Save this shift anyway?', 'wpaesp' );
		wp_send_json($results);    
		die();
	}

	if( !isset( $_POST['end'] ) || '' == $_POST['end'] ) {
		$results['action'] = 'stop';
		$results['message'] = __( 'You have not selected an end time.  Save this shift anyway?', 'wpaesp' );
		wp_send_json($results);    
		die();
	}

	$availability = wpaesp_check_employee_availability( $_POST['employee'], $_POST['day'], $_POST['start'], $_POST['end'] );
	if( true !== $availability ) {
		$employee = get_user_by( 'id', $_POST['employee'] );
		$results['action'] = 'stop';
		$results['message'] = sprintf( __( '%s is not available to work on %s between %s and %s.  Click "OK" to create the shift anyway, or "Cancel" to continue editing.', 'wpaesp' ), $employee->display_name, ucfirst( $availability['unavailable']['day'] ), $availability['unavailable']['start'], $availability['unavailable']['end'] );
		wp_send_json($results);    
		die();
	}

	$conflicts = wpaesp_check_employee_schedule_conflicts( $_POST['employee'], $_POST['day'], $_POST['start'], $_POST['end'], $_POST['post'] );
	if( true !== $conflicts ) {
		$employee = get_user_by( 'id', $_POST['employee'] );
		$results['action'] = 'stop';
		$results['message'] = sprintf( __( '%s is already scheduled for a shift at this time.  Click "OK" to create the shift anyway, or "Cancel" to continue editing.', 'wpaesp' ), $employee->display_name );
		wp_send_json($results);    
		die();
	}

	$results['action'] = 'go';
	$results['message'] = 'no conflicts!';
	wp_send_json($results);    
	die();
}
add_action( 'wp_ajax_wpaesp_check_for_schedule_conflicts_before_publish', 'wpaesp_check_for_schedule_conflicts_before_publish' );


// check that this shift does not fall within employee's regular unavailability time, as saved in user meta
function wpaesp_check_employee_availability( $employee_id, $day, $start_time, $end_time ) {
	// get the employee meta
	$unavailability = get_the_author_meta( 'unavailable', $employee_id );
	// if the employee does not have any unavailability saved, return true
	if( !is_array( $unavailability ) || empty( $unavailability ) ) {
		return true;
	} else { // if the employee does have unavailability
		// figure out what day of the week the shift date is
		$day_of_week = strtolower( date( 'l', strtotime( $day ) ) );
		foreach( $unavailability as $unavail ) {
			if( $unavail['day'] == $day_of_week ) {// if the day of the week is a day with unavailability
				// find the timeframe and see if they overlap
				$shift_start = str_replace( ':', '', $start_time );
				$shift_end = str_replace( ':', '', $end_time );
				$unavailibility_start = str_replace( ':', '', $unavail['start'] );
				$unavailability_end = str_replace( ':', '', $unavail['end'] );
				if( empty( $unavailibility_start ) || empty( $unavailability_end ) ) {
					$unavailable_time['unavailable'] = $unavail;
					$unavailable_time['employee'] = $employee_id;
					return $unavailable_time;
				}
				if( ( $shift_start >= $unavailibility_start && $shift_start <= $unavailability_end ) || 
					( $shift_end >= $unavailibility_start && $shift_end <= $unavailability_end ) || 
					( $shift_start <= $unavailibility_start && $shift_end >= $unavailability_end ) ) 
				{
					$unavailable_time['unavailable'] = $unavail;
					$unavailable_time['employee'] = $employee_id;
					return $unavailable_time;
				} 
			} 
		}

	}

	// we've gotten this far, so the employee must be available
	return true;

}

// check that the employee isn't scheduled for another shift at the same time
function wpaesp_check_employee_schedule_conflicts( $employee_id, $day, $start_time, $end_time, $post_id ) {
	// query to see if any shifts are assigned to the employee on this day
	$args = array( 
	    'post_type' => 'shift',
	    'post_status' => 'any',
	    'posts_per_page' => -1,
	    'meta_key' => '_wpaesm_date',
	    'meta_value' => $day, 
	    'connected_type' => 'shifts_to_employees',
	    'connected_items' => $employee_id
	);
	
	$conflicting_shifts = new WP_Query( $args );
	
	// The Loop
	if ( $conflicting_shifts->have_posts() ) {
		while ( $conflicting_shifts->have_posts() ) : $conflicting_shifts->the_post();
			$conflicting_shift = get_the_id();
			if( intval( $conflicting_shift ) !== intval( $post_id ) ) { // make sure conflicting shift is not the shift we are trying to save
				// check to see if the times overlap
				$shift_start = str_replace( ':', '', $start_time );
				$shift_end = str_replace( ':', '', $end_time );
				global $shift_metabox;
				$meta = $shift_metabox->the_meta();
				$conflict_start = str_replace( ':', '', $meta['starttime'] );
				$conflict_end = str_replace( ':', '', $meta['endtime'] );
				if( ( $shift_start >= $conflict_start && $shift_start <= $conflict_end ) || ( $shift_end >= $conflict_start && $shift_end <= $conflict_end ) || ( $shift_start <= $conflict_start && $shift_end >= $conflict_end ) ) {
					return $conflicting_shift;
				} 
			}
		endwhile;
		wp_reset_postdata();
		// we got this far, so there must not be any conflicts
		return true;
	} else {
		return true;
	}
}



// ------------------------------------------------------------------------
// SETTINGS
// ------------------------------------------------------------------------

function wpaesp_add_defaults() {

	$tmp = get_option('wpaesp_options');

	if( !isset( $tmp['hours'] ) ) {
		$tmp['hours'] = '40';
	}
	if( !isset( $tmp['otrate'] ) ) {
		$tmp['otrate'] = '1.5';
	}
	if( !isset( $tmp['calculate'] ) ) {
		$tmp['calculate'] = 'actual';
	}
	if( !isset( $tmp['mileage'] ) ) {
		$tmp['mileage'] = '.56';
	}
	if( !isset( $tmp['pro_license'] ) ) {
		$tmp['pro_license'] = '';
	}
	if( !isset( $tmp['avoid_conflicts'] ) ) {
		$tmp['avoid_conflicts'] = '1';
	}
	if( !isset( $tmp['notification_email'] ) ) {
		$tmp['notification_email'] = '';
	}

	update_option( 'wpaesp_options', $tmp );
}

function wpaesp_options_init() {

	add_settings_section(
		'pro_options_section', 
		__( 'Employee Scheduler Pro Settings', 'wpaesp' ), 
		'pro_options_section_callback', 
		'wpaesm_plugin_options'
	);

	add_settings_field( 
		'pro_license', 
		__( 'License Key', 'wpaesp' ), 
		'wpaesp_pro_license_render', 
		'wpaesm_plugin_options', 
		'pro_options_section',
		array(
			__( 'Enter the license key that you received when you purchased the Employee Scheduler Pro plugin.', 'wpaesp' )
		)
	);

	add_settings_field(
		'hours',
		__( 'Hours', 'wpaesp' ),
		'wpaesp_hours_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Enter the number of hours in the work week: if an employee works more hours, it will be considered overtime. You can change this setting per employee by editing the employee\'s user profile.', 'wpaesp' ),
		)
	);

	add_settings_field(
		'otrate',
		__( 'Overtime Rate', 'wpaesp' ),
		'wpaesp_otrate_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Enter the overtime pay rate as it relates to the normal pay rate. For instance, if you enter "1.5", overtime hours will be paid 1.5 times the normal hourly rate.', 'wpaesp' ),
		)
	);

	add_settings_field(
		'calculate',
		__( 'Payroll: Scheduled or Actual', 'wpaesp' ),
		'wpaesp_calculate_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Should payroll be calculated based on hours scheduled, or hours worked?', 'wpaesp' ),
		)
	);

	add_settings_field(
		'mileage',
		__( 'Mileage Reimbursement', 'wpaesp' ),
		'wpaesp_mileage_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'If you reimburse employees for mileage, enter the dollar amount reimbursed per mile, with no currency symbol.  Example: .56', 'wpaesp' ),
		)
	);

	add_settings_field(
		'avoid_conflicts',
		__( 'Check for Scheduling Conflicts', 'wpaesp' ),
		'wpaesp_avoid_conflicts_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'When you create shifts, check whether employees are already scheduled to work at that time, or whether they are unavailable.', 'wpaesp' )
		)
	);

	add_settings_field(
		'self_assign',
		__( 'Allow Employees to Pick Up Unassigned Shifts', 'wpaesp' ),
		'wpaesp_self_assign_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'If a shift appears on the schedule that is not assigned to an employee, let employees take the shift?  If this box is checked, employees will see a "take shift" option on unassigned shifts.', 'wpaesp' )
		)
	);

	add_settings_field(
		'allow_drop',
		__( 'Allow Employees to Drop Shifts', 'wpaesp' ),
		'wpaesp_allow_drop_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Check this box to give the employees the option to drop shifts.  Employees will see a "drop shift" option on their shifts, and then the shift will no longer be assigned to that employee.', 'wpaesp' )
		)
	);

	add_settings_field(
		'drop_lock',
		__( 'Drop Shift Timeframe', 'wpaesp' ),
		'wpaesp_drop_lock_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'How much advance notice, in hours, must employees give to drop shifts?  When the shift falls within this timeframe, the "drop shift" option will be disabled. Example (for two days\' notice): 48', 'wpaesp' )
		)
	);

	add_settings_field(
		'drop_notification',
		__( 'Notify Administrators of Dropped Shifts', 'wpaesp' ),
		'wpaesp_drop_notification_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Send an email notification to administrators/managers when an employee drops a shift.', 'wpaesp' )
		)
	);

	add_settings_field(
		'pick_up_notification',
		__( 'Notify Administrators of Picked Up Shifts', 'wpaesp' ),
		'wpaesp_pick_up_notification_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Send an email notification to administrators/managers when an employee picks up an unassigned shift.', 'wpaesp' )
		)
	);

	add_settings_field(
		'pick_up_confirmation',
		__( 'Send Email Confirmation to Employees', 'wpaesp' ),
		'wpaesp_pick_up_confirmation_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'Send an email confirmation to employees when they pick up a shift.', 'wpaesp' )
		)
	);

	add_settings_field(
		'drop_pick_notification_email',
		__( 'Notification Email', 'wpaesp' ),
		'wpaesp_drop_pick_notification_email_render',
		'wpaesm_plugin_options',
		'pro_options_section',
		array( 
			__( 'The email address(es) that will receive notifications of dropped/picked up shifts.  You can enter multiple email addresses separated by commas.', 'wpaesp' )
		)
	);

}

function wpaesp_pro_license_render( $args ) { 

	$options = get_option( 'wpaesm_options' );
	$status = get_option( 'wpaesp_license' );
	?>
	<span class="description"><?php echo $args[0]; ?></span><br />
	<input type='text' name='wpaesm_options[pro_license]' value='<?php echo $options['pro_license']; ?>'>
		<?php if( $status !== false && $status == 'valid' ) { ?>
			<span style="color:green;"><?php _e('active'); ?></span>
			<?php wp_nonce_field( 'wpaesp_license_activation_nonce', 'wpaesp_license_activation_nonce' ); ?>
			<input type="submit" class="button-secondary" name="wpaesp_license_deactivate" value="<?php _e('Deactivate License', 'wpaesp'); ?>"/>
		<?php } else {
			wp_nonce_field( 'wpaesp_license_activation_nonce', 'wpaesp_license_activation_nonce' ); ?>
			<input type="submit" class="button-secondary" name="wpaesp_license_activate" value="<?php _e('Activate License', 'wpaesp'); ?>"/>
		<?php } ?>
	<?php

}

function wpaesp_hours_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>
	<input type="number" min="0" step="any" size="5" name="wpaesm_options[hours]" value="<?php echo $options['hours']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_otrate_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="number" min="0" step="any" size="5" name="wpaesm_options[otrate]" value="<?php echo $options['otrate']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_mileage_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="number" min="0" step="any" name="wpaesm_options[mileage]" value="<?php echo $options['mileage']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_calculate_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[calculate]" type="radio" value="scheduled" <?php if (isset($options['calculate'])) { checked('scheduled', $options['calculate']); } ?> /> <?php _e('Calculate payroll based on hours scheduled', 'wpaesp'); ?></label><br />
	<label><input name="wpaesm_options[calculate]" type="radio" value="actual" <?php if (isset($options['calculate'])) { checked('actual', $options['calculate']); } ?> /> <?php _e('Calculate payroll based on hours actually worked', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_avoid_conflicts_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[avoid_conflicts]" type="checkbox" value="1" <?php if (isset($options['avoid_conflicts'])) { checked('1', $options['avoid_conflicts']); } ?> /> <?php _e('Check for scheduling conflicts', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_self_assign_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[self_assign]" type="checkbox" value="1" <?php if (isset($options['self_assign'])) { checked('1', $options['self_assign']); } ?> /> <?php _e('Allow Employees to Pick Up Unassigned Shifts', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_allow_drop_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[allow_drop]" type="checkbox" value="1" <?php if (isset($options['allow_drop'])) { checked('1', $options['allow_drop']); } ?> /> <?php _e('Allow Employees to Drop Shifts', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_drop_lock_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<input type="number" min="0" step=".5" name="wpaesm_options[drop_lock]" value="<?php echo $options['drop_lock']; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_drop_notification_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[drop_notification]" type="checkbox" value="1" <?php if (isset($options['drop_notification'])) { checked('1', $options['drop_notification']); } ?> /> <?php _e( 'Send dropped shift notification', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_pick_up_notification_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[pick_up_notification]" type="checkbox" value="1" <?php if (isset($options['pick_up_notification'])) { checked('1', $options['pick_up_notification']); } ?> /> <?php _e( 'Send picked-up shift notification', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }

function wpaesp_pick_up_confirmation_render( $args ) {
	$options = get_option( 'wpaesm_options' ); ?>

	<label><input name="wpaesm_options[pick_up_confirmation]" type="checkbox" value="1" <?php if (isset($options['pick_up_confirmation'])) { checked('1', $options['pick_up_confirmation']); } ?> /> <?php _e( 'Send confirmation email to employees when they pick up shifts.', 'wpaesp'); ?></label>
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }


function wpaesp_drop_pick_notification_email_render( $args ) {
	$options = get_option( 'wpaesm_options' ); 
	if( isset( $options['drop_pick_notification_email'] ) ) {
		$value = $options['drop_pick_notification_email'];
	} else {
		$value = get_bloginfo( 'admin_email' );
	} ?>

	<input type="text" size="57" name="wpaesm_options[drop_pick_notification_email]" value="<?php echo $value; ?>" />
	<br /><span class="description"><?php echo $args[0]; ?></span>
<?php }


function pro_options_section_callback(  ) { 

	// echo __( 'Thank you for using Employee Scheduler Pro!', 'wpaesp' );

}

function wpaesp_validate_options( $input ) {
	if( isset( $input['self_assign'] ) )
		$input['self_assign'] =  wp_filter_nohtml_kses($input['self_assign']);
	if( isset( $input['allow_drop'] ) )
		$input['allow_drop'] =  wp_filter_nohtml_kses($input['allow_drop']);
	if( isset( $input['drop_lock'] ) )
		$input['drop_lock'] =  wp_filter_nohtml_kses($input['drop_lock']);
	if( isset( $input['drop_notification'] ) )
		$input['drop_notification'] =  wp_filter_nohtml_kses($input['drop_notification']);
	if( isset( $input['pick_up_notification'] ) )
		$input['pick_up_notification'] =  wp_filter_nohtml_kses($input['pick_up_notification']);
	if( isset( $input['pick_up_confirmation'] ) )
		$input['pick_up_confirmation'] =  wp_filter_nohtml_kses($input['pick_up_confirmation']);
	if( isset( $input['drop_pick_notification_email'] ) )
		$input['drop_pick_notification_email'] =  wp_filter_nohtml_kses($input['drop_pick_notification_email']);

	return $input;
}
add_filter( 'wpaesm_validation_options_filter', 'wpaesp_validate_options' );

?>