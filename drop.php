<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// ------------------------------------------------------------------------
// ADD "DROP SHIFT" BUTTON TO SINGLE SHIFT VIEW
// ------------------------------------------------------------------------

function wpaesp_add_drop_shift_button( $content, $priority, $shift_id, $employee ) {

	$current_user = wp_get_current_user();
	if( $current_user->ID !== $employee ) {
		// this shift doesn't belong to the current user, so bail out now
		return $content;
	}

	$options = get_option( 'wpaesm_options' ); 
	if( !isset( $options['allow_drop'] ) || '1' !== $options['allow_drop'] ) {
		// employees aren't allowed to drop shifts, so bail out now
		return $content;
	}

	if( 'Unassigned' == $employee ) {
		// this shift doesn't have an employee, so bail out now
		return $content;
	}

	if( empty( $content ) ) {
		// there aren't any shifts today, so bail out now
		return $content;
	}

	if( isset( $_POST['form_name'] ) && "drop-shift" == ( $_POST['form_name'] ) ) {
		wpaesp_employee_drop_shift();
	}

	// make sure it is in the right timeframe
	// get the shift start time
	$start_date = get_post_meta( $shift_id, '_wpaesm_date', true );
	$start_time = get_post_meta( $shift_id, '_wpaesm_starttime', true );
	$timeframe = 3600 * $options['drop_lock'];
	$deadline = strtotime( $start_date . ' ' . $start_time ) - $timeframe;
	$now = time();
	if( $now >= $deadline ) {
		// the deadline has passed, so we can't drop shifts
		$disabled = 'disabled';
		$deadline_text = sprintf( __( 'The deadline for dropping shifts has passed.  You must provide at least %s hours advance notice to drop a shift.', 'wpaesp' ), $options['drop_lock'] );
	} else {
		// the deadline hasn't happened yet, so we can drop shifts
		$disabled = '';
		$date_format = get_option( 'date_format' );
		$time_format = get_option( 'time_format' );
		$deadline_text = sprintf( __( 'You have until %s to drop this shift.', 'wpaesp' ), date( $date_format . ' ' . $time_format, $deadline ) );
	}

	$drop_form = 
		'<p>' . $deadline_text . '</p>
		<form method="post" action="' . get_the_permalink() . '" id="drop-shift">
			<input type="hidden" name="form_name" value="drop-shift">
			<input type="hidden" name="shift_id" value="' . $shift_id . '">
			<input type="hidden" name="employee" value="' . $current_user->ID . '">
			<input name="wpaesp_drop_shift_nonce" id="wpaesp_drop_shift_nonce" type="hidden" value="' . wp_create_nonce( 'wpaesp_drop_shift_nonce' ) . '">
			<input type="submit" value="' . __( 'Drop Shift', 'wpaesp' ) . '" id="drop-shift"' . $disabled . '>
		</form>';

	$content = $content . apply_filters( 'wpaesp_filter_drop_shift_form', $drop_form );
	return $content;

}
add_filter( 'wpaesm_filter_single_shift_view', 'wpaesp_add_drop_shift_button', 10, 4 );


function wpaesp_employee_drop_shift() {
	if ( !wp_verify_nonce( $_POST['wpaesp_drop_shift_nonce'], 'wpaesp_drop_shift_nonce' ) ) {
        exit( 'Permission error.' );
    }

    // break the connection between the shift and employee
	p2p_type( 'shifts_to_employees' )->disconnect( $_POST['shift_id'], $_POST['employee'] );

	// give shift "unassigned" shift_status
	$unassigned = get_term_by( 'slug', 'unassigned', 'shift_status' );
	wp_set_post_terms( $_POST['shift_id'], $unassigned->term_id, 'shift_status', true );

	// if shift was "assigned", removed "assigned" shift status
	wp_remove_object_terms( $_POST['shift_id'], 'assigned', 'shift_status' );

	// save meta data to record that this happened
	wpaesp_save_shift_action_history( $_POST['shift_id'], $_POST['employee'], __( 'Drop shift', 'wpaesp' ) );

	do_action( 'wpaesp_drop_shift_action', $_POST['shift_id'], $_POST['employee'] );

    // refresh the page so that users see the new schedule
	wp_redirect( get_permalink() );
}

add_action( 'wpaesp_drop_shift_action', 'wpaesp_send_admin_drop_notification', 10, 3 );
function wpaesp_send_admin_drop_notification( $shift, $employee_id ) {

	$options = get_option( 'wpaesm_options' );
	if( 1 !== $options['drop_notification'] ) {
		return;
	}

	$options = get_option( 'wpaesm_options' );

	$employee_name = '';
	$employee_object = get_user_by( 'id', $employee_id );
	if( empty( $employee_object ) ) {
		$employee_name = __( 'Employee Unknown', 'wpaesp' );
	} else {
		$employee_name = $employee_object->display_name;
	}

	$from = wpaesm_email_from();

	if( isset( $options['drop_pick_notification_email'] ) ) {
		$to = $options['drop_pick_notification_email'];
	} else {
		$to = get_bloginfo( 'admin_email' );
	}

	$users_manager = new WP_User_Query( array(
		'connected_type' => 'manager_to_employee',
		'connected_items' => $employee_id,
	) );

	if ( ! empty( $users_manager->results ) ) {
		foreach ( $users_manager->results as $manager ) {
			$cc = $manager->user_email;
		}
	} else {
		$cc = '';
	}

	$subject = $employee_name . __( ' has just dropped a shift', 'wpaesp' );

	// gather all the shift information to put in the email
	$date = get_post_meta( $shift, '_wpaesm_date', true );
	if( empty( $date ) ) {
		$date = __( 'Date not available', 'wpaesp' );
	}

	$start = get_post_meta( $shift, '_wpaesm_starttime', true );
	if( empty( $start ) ) {
		$start = __( 'Start time not available', 'wpaesp' );
	}

	$end = get_post_meta( $shift, '_wpaesm_endtime', true );
	if( empty( $end ) ) {
		$end = __( 'End time not available', 'wpaesp' );
	}

	$jobs = get_posts( array(
		'connected_type' => 'shifts_to_jobs',
		'connected_items' => $shift,
		'nopaging' => true,
		'suppress_filters' => false
	) );
	if( !empty( $jobs ) ) {
		foreach( $jobs as $job_post ) {
			$job = get_the_title( $job_post );
		}
	}

	$message = 
	'<p>' . $employee_name . __( ' has just dropped the following shift', 'wpaesp' ) . ':<br />
		<strong>' . __( 'Date', 'wpaesp' ) . ': </strong>' . $date . '<br />
		<strong>' . __( 'Time', 'wpaesp' ) . ': </strong>' . $start . ' - ' . $end . '<br />';
		if( isset( $job ) ) {
			$message .= '<strong>' . __( 'Job', 'wpaesp' ) . ': </strong>' . $job . '<br />';
		}
	$message .= '
	</p>
	<p>
		<a href="' . get_the_permalink( $shift ) . '">' . __( 'View Shift Details', 'wpaesp' ) . '</a>
	</p>
	<p>
		<a href="' . get_edit_post_link( $shift ) . '">' . __( 'Edit Shift', 'wpaesp' ) . '</a>
	</p><br />
	<p>
		<small>' . 
		__( 'This email was sent by the Employee Scheduler plugin.  If you no longer wish to receive these emails, you can change your preferences on the Employee Scheduler settings page.', 'wpaesp' )
		. ' </small>
	</p>'; 

	wpaesm_send_email( $from, $to, $cc, $subject, $message );
}