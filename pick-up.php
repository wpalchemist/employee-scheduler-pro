<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// ------------------------------------------------------------------------
// ADD "TAKE SHIFT" BUTTON TO MASTER SCHEDULE AND SINGLE SHIFT VIEW
// ------------------------------------------------------------------------

function wpaesp_add_take_shift_button( $content, $priority, $shift_id, $employee ) {

	$options = get_option( 'wpaesm_options' ); 
	if( is_admin() ) {
		// we don't want to do this if we are in the dashboard, so bail out now
		return $content;
	}

	if( !isset( $options['self_assign'] ) || '1' !== $options['self_assign'] ) {
		// employees aren't allowed to take unassigned shifts, to bail out now
		return $content;
	}

	if( 'Unassigned' !== $employee ) {
		// this shift has an employee, so bail out now
		return $content;
	}

	if( empty( $content ) ) {
		// there aren't any shifts today, so bail out now
		return $content;
	}

	$current_user = wp_get_current_user();

	if( isset( $_POST['form_name'] ) && "pick-up-shift" == ( $_POST['form_name'] ) ) {
		if ( !wp_verify_nonce( $_POST['wpaesp_pick_up_shift_nonce'], 'wpaesp_pick_up_shift_nonce' ) ) {
	        exit( 'Permission error.' );
	    }

		wpaesp_employee_take_shift( intval( $_POST['shift_id'] ), intval( $_POST['employee'] ) );

		// refresh the page so that users see the new schedule
		wp_redirect( get_the_permalink() . wpaesp_add_week_to_permalink() );
		die(); // don't do this again
	}

	$pick_up_form = 
		'<form method="post" action="' . get_the_permalink() . wpaesp_add_week_to_permalink() . '" id="pick-up-shift">
			<input type="hidden" name="form_name" value="pick-up-shift">
			<input type="hidden" name="shift_id" value="' . $shift_id . '">
			<input type="hidden" name="employee" value="' . $current_user->ID . '">
			<input name="wpaesp_pick_up_shift_nonce" id="wpaesp_pick_up_shift_nonce" type="hidden" value="' . wp_create_nonce( 'wpaesp_pick_up_shift_nonce' ) . '">
			<input type="submit" value="' . __( 'Take Shift', 'wpaesp' ) . '" id="pick-up-shift">
		</form>';

	$content = $content . $pick_up_form;
	return $content;

}
add_filter( 'wpaesm_single_shift_cell', 'wpaesp_add_take_shift_button', 10, 4 );
add_filter( 'wpaesm_filter_single_shift_view', 'wpaesp_add_take_shift_button', 10, 4 );

function wpaesp_add_week_to_permalink() {
	if( isset( $_GET['week'] ) ) {
    	$thisweek = '?week=' . $_GET['week'];
    } else {
    	$thisweek = '';
    }

    return $thisweek;
}


function wpaesp_employee_take_shift( $shift, $employee ) {

    // connect the shift to the employee
    p2p_type( 'shifts_to_employees' )->connect( $shift, $employee, array(
	    'date' => current_time('mysql')
	) );

	// give shift "assigned" shift_status
	$assigned = get_term_by( 'slug', 'assigned', 'shift_status' );
	wp_set_post_terms( $shift, $assigned->term_id, 'shift_status', true );

	// if shift was "unassiged", removed "unassigned" shift status
	wp_remove_object_terms( $shift, 'unassigned', 'shift_status' );

	// save meta data to record that this happened
	wpaesp_save_shift_action_history( $shift, $employee, __( 'Take Shift', 'wpaesp') );

	// send email to manager, if needed
	$options = get_option( 'wpaesm_options' );
	if( 1 == $options['pick_up_notification'] ) {
		do_action( 'wpaesp_send_admin_pick_up_notification', $shift );
	}

	// send email to employee, if needed
	if( 1 == $options['pick_up_confirmation'] ) {
		do_action( 'wpaesp_send_employee_pick_up_confirmation', $shift );
	}

	do_action( 'wpaesp-take-shift-action', $shift, $employee );

}


function wpaesp_save_shift_action_history( $shift, $employee, $action ) {

	$ancient_history = get_post_meta( $shift, '_wpaesp_history' );
	$date = date( 'Y-m-d' );

	if( !empty( $ancient_history ) ) {
		$history_array = $ancient_history[0];
        $new_history = array(
				'date' => $date,
				'employee' => $employee,
				'action' => $action,
			);
        $history_array[] = $new_history;

        update_post_meta( $shift, '_wpaesp_history', $history_array ); 

    } else {
		$fields = array('_wpaesp_history');
		$str = $fields;
		update_post_meta( $shift, 'shift_history_fields', $str );

		$historical_event = array(
			array(
				'date' => $date,
				'employee' => $employee,
				'action' => $action,
				)
			);

		update_post_meta( $shift, '_wpaesp_history', $historical_event );
	}
}

add_action( 'wpaesp_send_admin_pick_up_notification', 'wpaesp_send_admin_pick_up_notification', 10, 2 );
function wpaesp_send_admin_pick_up_notification( $shift ) {
	$options = get_option( 'wpaesm_options' );

	$employee_name = '';
	$employees = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $shift
	) );

	foreach( $employees as $employee ) {
		$employee_name = $employee->display_name;
	}

	$from = wpaesm_email_from();

	if( isset( $options['drop_pick_notification_email'] ) ) {
		$to = $options['drop_pick_notification_email'];
	} else {
		$to = get_bloginfo( 'admin_email' );
	}

	$users_manager = new WP_User_Query( array(
		'connected_type' => 'manager_to_employee',
		'connected_items' => $employee->ID,
	) );

	if ( ! empty( $users_manager->results ) ) {
		foreach ( $users_manager->results as $manager ) {
			$cc = $manager->user_email;
		}
	} else {
		$cc = '';
	}

	$subject = $employee_name . __( ' has just taken a shift', 'wpaesp' );

	// gather all the shift information to put in the email
	$date = get_post_meta( $shift, '_wpaesm_date', true );
	if( empty( $date ) ) {
		$date = __( 'Date not available', 'wpaesp' );
	}

	$start = get_post_meta( $shift, '_wpaesm_starttime', true );
	if( empty( $start ) ) {
		$start = __( 'Start time not available', 'wpaesp' );
	}

	$end = get_post_meta( $shift, '_wpaesm_endtime', true );
	if( empty( $end ) ) {
		$end = __( 'End time not available', 'wpaesp' );
	}

	$jobs = get_posts( array(
		'connected_type' => 'shifts_to_jobs',
		'connected_items' => $shift,
		'nopaging' => true,
		'suppress_filters' => false
	) );
	if( !empty( $jobs ) ) {
		foreach( $jobs as $job_post ) {
			$job = get_the_title( $job_post );
		}
	}

	$message = 
	'<p>' . $employee_name . __( ' has just signed up to work the following shift', 'wpaesp' ) . ':<br />
		<strong>' . __( 'Date', 'wpaesp' ) . ': </strong>' . $date . '<br />
		<strong>' . __( 'Time', 'wpaesp' ) . ': </strong>' . $start . ' - ' . $end . '<br />';
		if( isset( $job ) ) {
			$message .= '<strong>' . __( 'Job', 'wpaesp' ) . ': </strong>' . $job . '<br />';
		}
	$message .= '
	</p>
	<p>
		<a href="' . get_the_permalink( $shift ) . '">' . __( 'View Shift Details', 'wpaesp' ) . '</a>
	</p>
	<p>
		<a href="' . get_edit_post_link( $shift ) . '">' . __( 'Edit Shift', 'wpaesp' ) . '</a>
	</p><br />
	<p>
		<small>' . 
		__( 'This email was sent by the Employee Scheduler plugin.  If you no longer wish to receive these emails, you can change your preferences on the Employee Scheduler settings page.', 'wpaesp' )
		. ' </small>
	</p>'; 


	wpaesm_send_email( $from, $to, $cc, $subject, $message );
}

add_action( 'wpaesp_send_employee_pick_up_confirmation', 'wpaesp_send_employee_pick_up_confirmation', 10, 2 );
function wpaesp_send_employee_pick_up_confirmation( $shift ) {
	$options = get_option( 'wpaesm_options' );

	$employee_name = '';
	$employees = get_users( array(
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $shift
	) );

	foreach( $employees as $employee ) {
		$employee_name = $employee->display_name;
	}

	$from = wpaesm_email_from();

	$to = $employee->user_email;

	$cc = '';

	$subject = __( 'Your Shift Details', 'wpaesp' );

	// gather all the shift information to put in the email
	$date = get_post_meta( $shift, '_wpaesm_date', true );
	if( empty( $date ) ) {
		$date = __( 'Date not available', 'wpaesp' );
	}

	$start = get_post_meta( $shift, '_wpaesm_starttime', true );
	if( empty( $start ) ) {
		$start = __( 'Start time not available', 'wpaesp' );
	}

	$end = get_post_meta( $shift, '_wpaesm_endtime', true );
	if( empty( $end ) ) {
		$end = __( 'End time not available', 'wpaesp' );
	}

	$jobs = get_posts( array(
		'connected_type' => 'shifts_to_jobs',
		'connected_items' => $shift,
		'nopaging' => true,
		'suppress_filters' => false
	) );
	if( !empty( $jobs ) ) {
		foreach( $jobs as $job_post ) {
			$job = get_the_title( $job_post );
		}
	}

	$message = 
	'<p>' . __( 'You have just signed up to work the following shift', 'wpaesp' ) . ':<br />
		<strong>' . __( 'Date and Time', 'wpaesp' ) . ': </strong>' . $date . ', ' . $start . ' - ' . $end . '<br />';
		if( isset( $job ) ) {
			$message .= '<strong>' . __( 'Job', 'wpaesp' ) . ': </strong>' . $job . '<br />';
		}
	$message .= '
	</p>
	<p>
		<a href="' . get_the_permalink( $shift ) . '">' . __( 'View Shift Details', 'wpaesp' ) . '</a>
	</p>'; 


	wpaesm_send_email( $from, $to, $cc, $subject, $message );
}


// ------------------------------------------------------------------------
// UNASSIGNED SHIFTS SHORTCODE
// ------------------------------------------------------------------------

function wpaesp_create_unassigned_shifts_shortcode( $atts ) {
	// unfortunately, wpp2p doesn't have a way to search for shifts that don't have a connection, so we can only search for shifts in the "unassigned" category

	// Attributes
	extract( shortcode_atts(
		array(
			'number' => '50',
		), $atts )
	);

	$unassigned_shifts = '';

	if( is_user_logged_in() && ( wpaesm_check_user_role( 'employee' ) || wpaesm_check_user_role( 'administrator' ) ) ) { // only show this if current user is admin or employee

		// process the form if we need to
		if( isset( $_POST['form_name'] ) && "pick-up-many-shifts" == ( $_POST['form_name'] ) ) {
			if ( !wp_verify_nonce( $_POST['wpaesp_pick_up_shift_nonce'], 'wpaesp_pick_up_shift_nonce' ) ) {
		        exit( 'Permission error.' );
		    }
		    if( !empty( $_POST['take-shift'] ) ) {
		    	foreach( $_POST['take-shift'] as $shift ) {
		    		wpaesp_employee_take_shift( $shift, $_POST['employee'] );
		    	}
		    }

		    // refresh the page so that users see the new schedule
			wp_redirect( get_the_permalink() . wpaesp_add_week_to_permalink() );
			die(); // don't do this again
		}

		$today = date('Y-m-d');

		$args = array( 
		    'post_type' => 'shift',  
		    'posts_per_page' => $number, 
		    'meta_key'   => '_wpaesm_date',
			'order'      => 'ASC',
		    // 'paged' => get_query_var('paged'),
		    'meta_query' => array(
		       array(
		         'key' => '_wpaesm_date',
		         'value' => $today,
		         'type' => 'CHAR',
		         'compare' => '>=',
		       ),
		    ),
		    'tax_query' => array(
				array(
					'taxonomy' => 'shift_status',
					'field'    => 'slug',
					'terms'    => 'unassigned',
				),
			),
		);

		$unassigned_query = new WP_Query( $args );
		
		// The Loop
		if ( $unassigned_query->have_posts() ) :
			$unassigned_shifts .= 
			'<form id="unassigned-shifts" method="post" action="' . get_the_permalink() . '">
				<table>
					<tr>
						<th>' . __( 'Select Shift', 'wpaesp' ) . '</th>
						<th>' . __( 'Date', 'wpaesp' ) . '</th>
						<th>' . __( 'Time', 'wpaesp' ) . '</th>
						<th>' . __( 'Job', 'wpaesp' ) . '</th>
						<th>' . __( 'Details', 'wpaesp' ) . '</th>
					</tr>';
					while ( $unassigned_query->have_posts() ) : $unassigned_query->the_post();
						// get all the values we need for the table
						global $shift_metabox; // get metabox data
						$meta = $shift_metabox->the_meta(); 

						// get shift date
						if( isset( $meta['date'] ) ) {
							$shift_date = $meta['date'];
						} else {
							$shift_date = __( 'No date set', 'wpaesp' );
						}

						// get shift start time
						if( isset( $meta['starttime'] ) ) {
							$shift_start = date( "g:i a", strtotime( $meta['starttime'] ) );
						} else {
							$shift_start = __( 'No start time', 'wpaesp' ); 
						}

						// get shift end time
						if( isset( $meta['endtime'] ) ) {
							$shift_end = date( "g:i a", strtotime( $meta['endtime'] ) );
						} else {
							$shift_end = __( 'No start time', 'wpaesp' ); 
						}

						// get job name
						$jobs = get_posts( array(
						  'connected_type' => 'shifts_to_jobs',
						  'connected_items' => get_the_id(),
						  'nopaging' => true,
						  'suppress_filters' => false
						) );
						foreach($jobs as $job) {
							$jobname = $job->post_title;
						}

						// make the table
						$unassigned_shifts .= 
						'<tr>
							<td>
								<input type="checkbox" name="take-shift[]" value="' . get_the_id() . '">
							</td>
							<td>'
								. $shift_date .
							'</td>
							<td>'
								. $shift_start . ' - ' . $shift_end .
							'</td>
							<td>'
								. $jobname .
							'</td>
							<td>
								<a href="' . get_the_permalink() . '">' . __( 'View Shift Details', 'wpaesp' ) . '</a>
							</td>
						</tr>';
					endwhile;
					$unassigned_shifts .= 
				'</table>
				<input name="wpaesp_pick_up_shift_nonce" id="wpaesp_pick_up_shift_nonce" type="hidden" value="' . wp_create_nonce( 'wpaesp_pick_up_shift_nonce' ) . '">
				<input type="hidden" name="employee" value="' . get_current_user_id() . '">
				<input type="hidden" name="form_name" value="pick-up-many-shifts">
				<input type="submit" value="' . __( 'Take Selected Shifts', 'wpaesm' ) . '">
			</form>';
		endif;
		wp_reset_postdata();
	} else {
		// need to log in
		$unassigned_shifts = "<p>" . __( 'You must be logged in to view this page.', 'wpaesp' ) . "</p>";
		$args = array(
	        'echo' => false,
		); 
		$unassigned_shifts .= wp_login_form( $args );
	}
	
	return $unassigned_shifts;
}
add_shortcode( 'unassigned_shifts', 'wpaesp_create_unassigned_shifts_shortcode' );


// ------------------------------------------------------------------------
// SHOW UNASSIGNED SHIFTS ON SINGLE JOB
// ------------------------------------------------------------------------

add_filter( 'the_content', 'wpaesp_unassigned_shifts_on_job_view' );

function wpaesp_unassigned_shifts_on_job_view( $content ) {

	if( is_singular( 'job' ) && is_main_query() ) {
		if( is_user_logged_in() && ( wpaesm_check_user_role( 'employee' ) || wpaesm_check_user_role( 'administrator' ) ) ) {

			$options = get_option( 'wpaesm_options' ); 
			if( is_admin() ) {
				// we don't want to do this if we are in the dashboard, so bail out now
				return $content;
			}

			if( !isset( $options['self_assign'] ) || '1' !== $options['self_assign'] ) {
				// employees aren't allowed to take unassigned shifts, to bail out now
				return $content;
			}

			// process the form if we need to
			if( isset( $_POST['form_name'] ) && "pick-up-many-shifts" == ( $_POST['form_name'] ) ) {
				if ( !wp_verify_nonce( $_POST['wpaesp_pick_up_shift_nonce'], 'wpaesp_pick_up_shift_nonce' ) ) {
			        exit( 'Permission error.' );
			    }
			    if( !empty( $_POST['take-shift'] ) ) {
			    	foreach( $_POST['take-shift'] as $shift ) {
			    		wpaesp_employee_take_shift( $shift, $_POST['employee'] );
			    	}
			    }

			    // refresh the page so that users see the new schedule
				wp_redirect( get_the_permalink() . wpaesp_add_week_to_permalink() );
				die(); // don't do this again
			}
		
			$today = date('Y-m-d');

			$take_job = '';

			$args = array( 
			    'post_type' => 'shift',  
			    'posts_per_page' => -1, 
			    'meta_key'   => '_wpaesm_date',
				'order'      => 'ASC',
			    // 'paged' => get_query_var('paged'),
			    'meta_query' => array(
			       array(
			         'key' => '_wpaesm_date',
			         'value' => $today,
			         'type' => 'CHAR',
			         'compare' => '>=',
			       ),
			    ),
			    'tax_query' => array(
					array(
						'taxonomy' => 'shift_status',
						'field'    => 'slug',
						'terms'    => 'unassigned',
					),
				),
				'connected_type' => 'shifts_to_jobs',
				'connected_items' => get_the_id(),
			);

			$unassigned_query = new WP_Query( $args );

			// $take_job .= '<pre>' . print_r($unassigned_query,true) . '</pre>';
			
			// The Loop
			if ( $unassigned_query->have_posts() ) :
				$take_job .= 
				'<form id="unassigned-shifts" method="post" action="' . get_the_permalink() . '">
					<table>
						<tr>
							<th><input type="checkbox" onClick="checkAll(this)" />' . __( 'Select All', 'wpaesp' ) . '</th>
							<th>' . __( 'Date', 'wpaesp' ) . '</th>
							<th>' . __( 'Time', 'wpaesp' ) . '</th>
							<th>' . __( 'Details', 'wpaesp' ) . '</th>
						</tr>';
						while ( $unassigned_query->have_posts() ) : $unassigned_query->the_post();
							// get all the values we need for the table
							global $shift_metabox; // get metabox data
							$meta = $shift_metabox->the_meta(); 

							// get shift date
							if( isset( $meta['date'] ) ) {
								$shift_date = $meta['date'];
							} else {
								$shift_date = __( 'No date set', 'wpaesp' );
							}

							// get shift start time
							if( isset( $meta['starttime'] ) ) {
								$shift_start = date( "g:i a", strtotime( $meta['starttime'] ) );
							} else {
								$shift_start = __( 'No start time', 'wpaesp' ); 
							}

							// get shift end time
							if( isset( $meta['endtime'] ) ) {
								$shift_end = date( "g:i a", strtotime( $meta['endtime'] ) );
							} else {
								$shift_end = __( 'No start time', 'wpaesp' ); 
							}

							// make the table
							$take_job .= 
							'<tr>
								<td>
									<input type="checkbox" name="take-shift[]" value="' . get_the_id() . '">
								</td>
								<td>'
									. $shift_date .
								'</td>
								<td>'
									. $shift_start . ' - ' . $shift_end .
								'</td>
								<td>
									<a href="' . get_the_permalink() . '">' . __( 'View Shift Details', 'wpaesp' ) . '</a>
								</td>
							</tr>';
						endwhile;
						$take_job .= 
					'</table>
					<input name="wpaesp_pick_up_shift_nonce" id="wpaesp_pick_up_shift_nonce" type="hidden" value="' . wp_create_nonce( 'wpaesp_pick_up_shift_nonce' ) . '">
					<input type="hidden" name="employee" value="' . get_current_user_id() . '">
					<input type="hidden" name="form_name" value="pick-up-many-shifts">
					<input type="submit" value="' . __( 'Take Selected Shifts', 'wpaesm' ) . '">
				</form>
				<script language="JavaScript">
					function checkAll(source) {
					  checkboxes = document.getElementsByName("take-shift[]");
					  for(var i=0, n=checkboxes.length;i<n;i++) {
					    checkboxes[i].checked = source.checked;
					  }
					}
				</script>';
			endif;
			wp_reset_postdata();

			$content = $content . $take_job;
			
		}
	}

	return $content;

}