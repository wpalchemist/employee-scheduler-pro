jQuery(document).ready(function ($) {

    jQuery('.starttime').datetimepicker({
      datepicker:false,
      format:'H:i',
      step: 15,
    });
    jQuery('.endtime').datetimepicker({
      datepicker:false,
      format:'H:i',
      step: 15,
    });

    var flag_ok = false;
    $('#publish').on('click', function (e) {
        // only do this if avoid_conflicts option is on
        if( "1" == wpaesp_shiftajax.avoid_conflicts ) {
            if ( ! flag_ok ) {
                $('#confirm-availability').show();
                e.preventDefault();
                
                var url = wpaesp_shiftajax.ajaxurl;
                var shift = $('#post_ID').val();
                var day = $('#thisdate').val();
                var start = $('#starttime').val();
                var end = $('#endtime').val();
                var post = $('#post_ID').val();
                // there is probably a better way to get the employee
                var employeeurl = $("a[href*='user-edit.php?user_id']").attr('href');
                if (!employeeurl) {
                    employee = '';
                } else {
                    employee = employeeurl.substr(employeeurl.indexOf("=") + 1);
                }
                // console.log(employee);

                var data = {
                    'action': 'wpaesp_check_for_schedule_conflicts_before_publish',
                    'shift': shift,
                    'day': day,
                    'start': start,
                    'end': end,
                    'employee': employee,
                    'post': post
                };

                $.post(url, data, function (response) {
                    // console.log(response);
                    $('#confirm-availability').hide();
                    if( response.action == 'go' ) {
                        // there aren't any scheduling conflicts, so we can publish the post
                        flag_ok = true;
                        $('#publish').trigger('click');
                    } else {
                        if (confirm(response.message)) {
                            flag_ok = true;
                            $('#publish').trigger('click');
                        } else {
                            // do nothing
                        }
                    }
                });
            }
        }

    });

    var repeatingTemplate = jQuery('#date-time-template').html();

    // Add a new repeating section
    jQuery('.repeat').click(function(e){
        e.preventDefault();
        var repeating = jQuery(repeatingTemplate);
        var lastRepeatingGroup = jQuery('.repeating').last();
        var idx = lastRepeatingGroup.index();
        var attrs = ['for', 'id', 'name'];
        var tags = repeating.find('input, label, select');
        tags.each(function() {
            var section = jQuery(this);
            jQuery.each(attrs, function(i, attr) {
                var attr_val = section.attr(attr);
                if (attr_val) {
                    section.attr(attr, attr_val.replace(/unavailable\[\d+\]\[/, 'unavailable\['+(idx + 1)+'\]\['))
                }
            })
        });


        lastRepeatingGroup.after(repeating);
        repeating.find('.starttime').datetimepicker({
            datepicker:false,
            format:'H:i',
            step: 15,
        });
        repeating.find('.endtime').datetimepicker({
            datepicker:false,
            format:'H:i',
            step: 15,
        });
    });

    jQuery('body').on('click', 'a.remove', function(e){
        e.preventDefault();
        jQuery(this).closest('.repeating').remove();
    });

    var availabilityTemplate = jQuery('#availability-template').html();

    jQuery('.repeat-availability').click(function(e){
        e.preventDefault();
        var repeating = jQuery(availabilityTemplate);
        var lastRepeatingGroup = jQuery('.repeating-availability').last();
        var idx = lastRepeatingGroup.index();
        var attrs = ['for', 'id', 'name'];
        var tags = repeating.find('input, label, select');
        tags.each(function() {
            var section = jQuery(this);
            jQuery.each(attrs, function(i, attr) {
                var attr_val = section.attr(attr);
                if (attr_val) {
                    section.attr(attr, attr_val.replace(/availability\[\d+\]\[/, 'availability\['+(idx + 1)+'\]\['))
                }
            })
        });


        lastRepeatingGroup.after(repeating);
        repeating.find('.starttime').datetimepicker({
            datepicker:false,
            format:'H:i',
            step: 15,
        });
        repeating.find('.endtime').datetimepicker({
            datepicker:false,
            format:'H:i',
            step: 15,
        });
    });

    jQuery('body').on('click', 'a.remove-availability', function(e){
        e.preventDefault();
        jQuery(this).closest('.repeating-availability').remove();
    });


});