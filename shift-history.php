<div class="my_meta_control" id="shiftinfo">

<?php _e( 'Record of when employees picked up and dropped shifts.', 'wpaesp' ); ?>

<?php while( $mb->have_fields_and_multi( 'history' ) ): ?>
<?php $mb->the_group_open(); ?>

	<p>
		<?php $mb->the_field( 'date' ); ?>
		<strong><?php _e( 'Date', 'wpaesp' ); ?>: </strong> <?php echo $mb->the_value(); ?>
		<input type="hidden" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/><br />

		<?php $mb->the_field( 'employee' );
		$employee = get_user_by( 'id', $mb->get_the_value() ); 
		if( empty( $employee ) ) {
			$employee_name = '';
		} else {
			$employee_name = $employee->user_nicename;
		} ?>
		<strong><?php _e( 'Employee', 'wpaesp' ); ?>: </strong> <?php echo $employee_name; ?>
		<input type="hidden" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/><br />

		<?php $mb->the_field( 'action' ); ?>
		<strong><?php _e( 'Activity', 'wpaesp' ); ?>: </strong> <?php echo $mb->the_value(); ?>
		<input type="hidden" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/><br />
	</p>

<?php $mb->the_group_close(); ?>
<?php endwhile; ?>

</div>
