<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// ------------------------------------------------------------------------
// FILTER SHIFTS                                        
// ------------------------------------------------------------------------

function wpaesp_add_filter_pages() {
	add_submenu_page( 'edit.php?post_type=shift', 'Filter Shifts', 'Filter Shifts', 'manage_options', 'filter-shifts', 'wpaesp_filter_shifts' );
	add_submenu_page( 'edit.php?post_type=expense', 'Filter Expenses', 'Filter Expenses', 'manage_options', 'filter-expenses', 'wpaesp_filter_expenses' );
}
add_action('admin_menu', 'wpaesp_add_filter_pages');


function wpaesp_filter_shifts() { ?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h1><?php _e('Filter Shifts', 'wpaesp'); ?></h1>

		
		<form method='post' action='<?php echo admin_url( 'admin.php?page=filter-shifts'); ?>' id='filter-shifts'>
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e('Select Date Range:', 'wpaesp'); ?></th>
					<td>
						<?php _e( 'From', 'wpaesp' ); ?> <input type="text" size="10" name="thisdate" id="thisdate" value="" /> 
						<?php _e( 'to', 'wpaesp' ); ?> <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Type:', 'wpaesp'); ?></th>
					<td>
						<select name="type" id="type">
							<option value=""> </option>
							<?php $shifttypes = get_terms( 'shift_type', 'hide_empty=0&orderby=name' );
							foreach ( $shifttypes as $type ) { ?>
								<option value="<?php echo $type->slug; ?>" ><?php echo $type->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Shift Status:', 'wpaesp'); ?></th>
					<td>
						<select name="status" id="status">
							<option value=""> </option>
							<?php $statuses = get_terms( 'shift_status', 'hide_empty=0&orderby=name' );
							foreach ( $statuses as $status ) { ?>
								<option value="<?php echo $status->slug; ?>" ><?php echo $status->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Employee:', 'wpaesp'); ?></th>
					<td>
						<select name="employee">
							<option value=""></option>
							<?php $employees = array_merge( get_users( 'role=employee&orderby=nicename' ), get_users( 'role=administrator&orderby=nicename' ) );
							usort( $employees, 'wpaesp_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<option value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Manager:', 'wpaesp'); ?></th>
					<td>
						<select name="manager">
							<option value=""></option>
							<?php $managers = get_users( 'role=es_manager&orderby=nicename' );
							usort( $managers, 'wpaesp_alphabetize' );
							foreach ( $managers as $manager ) { ?>
								<option value="<?php echo $manager->ID; ?>" ><?php echo $manager->display_name; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e('Job:', 'wpaesp'); ?></th>
					<td>
						<?php $args = array( 
						    'post_type' => 'job',  
						    'posts_per_page' => -1,  
						    'orderby' => 'name',
						    'order' => 'asc',
						);

						$jobquery = new WP_Query( $args );

						if ( $jobquery->have_posts() ) : ?>
							<select name="job">
								<option value=""></option>
								<?php while ( $jobquery->have_posts() ) : $jobquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Filter Shifts', 'wpaesp' ); ?>" />
			</p>
		</form>

		<?php if($_POST) { 

			if( isset( $_POST['thisdate'] ) && isset( $_POST['repeatuntil'] ) && ( $_POST['thisdate'] > $_POST['repeatuntil'] ) ) {
				$message = __( 'The end date must be after the begin date.', 'wpaesp');
				wp_die( $message );
			}

			// our basic args
			$args = array( 
				'post_type' => 'shift',
				'posts_per_page' => -1,
				);

			$criteria = '';

			// add start date and end date to args
			if( isset( $_POST['thisdate'] ) || isset( $_POST['repeatuntil'] ) ) {
				$args['meta_query'] = array();
				if( isset( $_POST['thisdate'] ) && '____-__-__' !== $_POST['thisdate'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['thisdate'],
				         'type' => 'CHAR',
				         'compare' => '>='
						);
					$criteria .= '<li>' . __( 'Begin date: ', 'wpaesp' ) . $_POST['thisdate'] . '</li>';
				}
				
				if( isset( $_POST['repeatuntil'] ) && '____-__-__' !== $_POST['repeatuntil'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['repeatuntil'],
				         'type' => 'CHAR',
				         'compare' => '<='
						);
					$criteria .= '<li>' . __( 'End date: ', 'wpaesp' ) . $_POST['repeatuntil'] . '</li>';
				}
			}

			// add shift type and shift status to args
			if( ( isset( $_POST['status'] ) && '' !== $_POST['status'] ) || ( isset( $_POST['type'] ) && '' !== $_POST['type'] ) ) {
				$args['tax_query'] = array(
					'relation' => 'AND',
					);
				if( isset( $_POST['status'] ) && '' !== $_POST['status'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'shift_status',
				        'field' => 'slug',
				        'terms' => $_POST['status'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Status: ', 'wpaesp' ) . $_POST['status'] . '<li>';
				}
				if( isset( $_POST['type'] ) && '' !== $_POST['type'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'shift_type',
				        'field' => 'slug',
				        'terms' => $_POST['type'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Type: ', 'wpaesp' ) . $_POST['type'] . '<li>';
				}
			}

			// add employee and job to args
			$need_additional_job_search = false;
			if( ( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) && ( isset( $_POST['job'] ) && '' !== $_POST['job'] ) ) {
				// we have both job and employee, but for now we're just going to search for employee
				if( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
					$args['connected_type'] = 'shifts_to_employees';
					$args['connected_items'] = $_POST['employee'];
					$criteria .= '<li>' . __( 'Employee: ', 'wpaesm' ) . $_POST['employee'] . '<li>';
				}

				// we need to search for job later
				$need_additional_job_search = true;
				$criteria .= '<li>' . __( 'Job: ', 'wpaesm' ) . $_POST['job'] . '<li>';

			} elseif( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
				// add employee to args
				$args['connected_type'] = 'shifts_to_employees';
				$args['connected_items'] = $_POST['employee'];
				$criteria .= '<li>' . __( 'Employee: ', 'wpaesm' ) . $_POST['employee'] . '<li>';
			} elseif( isset( $_POST['job'] ) && '' !== $_POST['job'] ) {
				// add job to args
				$args['connected_type'] = 'shifts_to_jobs';
				$args['connected_items'] = $_POST['job'];
				$criteria .= '<li>' . __( 'Job: ', 'wpaesm' ) . $_POST['job'] . '<li>';
			}

			// add manager to args
			if( isset( $_POST['manager'] ) && '' !== $_POST['manager'] ) {
				$manager_obj = get_user_by( 'id', $_POST['manager'] );
				if( $manager_obj ) {
					$managers_employees = new WP_User_Query( array(
						'connected_type'      => 'manager_to_employee',
						'connected_items'     => $manager_obj->ID,
						'connected_direction' => 'to',
					) );

					if ( ! empty( $managers_employees->results ) ) {
						$employee_ids = array();
						foreach( $managers_employees->results as $employee ) {
							$employee_ids[] = $employee->ID;
						}
						if( !empty( $employee_ids ) ) {
							$args['connected_type'] = 'shifts_to_employees';
							$args['connected_items'] = $employee_ids;
						}
					}
				}
			}

			echo '<p class="criteria">' . __( 'You searched for: ', 'wpaesp' ) . '<ul>' . $criteria . '</ul></p>' ;

			$filter = new WP_Query( $args );
			
			// The Loop
			if ( $filter->have_posts() ) { ?>
				<table id="filtered-shifts" class="wp-list-table widefat fixed posts">
					<thead>
						<tr>
							<th data-sort='string'><span><?php _e( 'Shift', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Date', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Time', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Employee', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Job', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Status', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Type', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Hours Scheduled/Worked', 'wpaesp' ); ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $filter->have_posts() ) : $filter->the_post(); 
							$postid = get_the_id();
							global $shift_metabox;
							$meta = $shift_metabox->the_meta(); 
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'shifts_to_employees',
								'connected_items' => $postid,
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employeeid = $user->ID;
								}
							}
							// get job associated with this shift
							$jobs = get_posts( array(
								'connected_type' => 'shifts_to_jobs',
								'connected_items' => $postid,
								'nopaging' => true,
								'suppress_filters' => false
							) );
							if( isset( $jobs ) ) { 
								foreach($jobs as $job) {
									$jobname = $job->post_title;
									$jobid = $job->ID;
								}
							}

							// do additional job filter if required 
							if( true == $need_additional_job_search ) {
								if( !isset( $jobid ) ) {
									continue;
								}
								if( intval( $jobid ) !== intval( $_POST['job'] ) ) {
									continue;
								}
							}
							?>
							<tr>
								<td class="title">	
									<?php the_title(); ?><br />
									<a href="<?php echo get_edit_post_link(); ?>"><?php _e( 'Edit', 'wpaesp' ); ?></a> |
									<a href="<?php the_permalink(); ?>"><?php _e( 'View', 'wpaesp' ); ?></a>
								</td>
								<td class="date">
									<?php if( isset( $meta['date'] ) ) {
										echo $meta['date'];
									} ?>
								</td>
								<td class="time">
									<?php if( isset( $meta['starttime'] ) && isset( $meta['endtime'] ) ){
										echo $meta['starttime']; ?> - <?php echo $meta['endtime'];
									} ?>
								</td>
								<td class="employee">
									<?php if( isset( $employeeid ) ) { ?>
										<a href="<?php echo get_edit_user_link( $employeeid ); ?>"><?php echo $employee; ?></a>
										<?php unset( $employeeid);
									} ?>
								</td>
								<td class="job">
									<?php if( isset( $jobid ) ) { ?>
										<a href="<?php echo get_edit_post_link( $jobid ); ?>"><?php echo $jobname; ?></a>
									<?php unset( $jobid);
									} ?>
								</td>
								<td class="status">
									<?php $statuslist =  wp_get_post_terms( $postid, 'shift_status' );
									if( isset( $statuslist ) ) {
										foreach($statuslist as $status) {  ?>
											<a href="<?php echo admin_url( 'edit.php?shift_status='.$status->slug.'&post_type=shift' ); ?>"><?php echo $status->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="type">
									<?php $typelist =  wp_get_post_terms( $postid, 'shift_type' );
									if( isset( $typelist ) ) {
										foreach($typelist as $type) {  ?>
											<a href="<?php echo admin_url( 'edit.php?shift_type='.$type->slug.'&post_type=shift' ); ?>"><?php echo $type->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="hours">
									<?php if( isset( $meta['starttime'] ) && $meta['starttime'] !== '____-__-__' && isset( $meta['endtime'] ) && $meta['endtime'] !== '____-__-__') {
										$startTime = date_create( date( "H:i",strtotime( $meta['starttime'] ) ) );
										$endTime = date_create( date( "H:i",strtotime( $meta['endtime'] ) ) );
										$timeInterval=date_diff($startTime, $endTime);
										$getHours=$timeInterval->format('%h');
										$getMinutes=$timeInterval->format('%I');
										$duration=$getHours.":".$getMinutes;
										_e('Scheduled: ' . $duration . '<br />', 'wpaesp');
									} ?>
									<?php if( isset( $meta['clockin'] ) && $meta['clockin'] !== '____-__-__' && isset( $meta['clockout'] ) && $meta['clockout'] !== '____-__-__') {
										$startTime = date_create( date( "H:i",strtotime( $meta['clockin'] ) ) );
										$endTime = date_create( date( "H:i",strtotime( $meta['clockout'] ) ) );
										$timeInterval=date_diff($startTime, $endTime);
										$getHours=$timeInterval->format('%h');
										$getMinutes=$timeInterval->format('%I');
										$duration=$getHours.":".$getMinutes;
										_e('Worked: ' . $duration . '<br />', 'wpaesp');
									} ?>
								</td>
							</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			<?php } else {
				_e( 'No results found.', 'wpaesp' );
			} 
			
			// Reset Post Data
			wp_reset_postdata();

		} ?>


	</div>
<?php } 


// ------------------------------------------------------------------------
// FILTER EXPENSES                          
// ------------------------------------------------------------------------

function wpaesp_filter_expenses() { ?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h1><?php _e('Filter Expenses', 'wpaesp'); ?></h1>

		
		<form method='post' action='<?php echo admin_url( 'admin.php?page=filter-expenses'); ?>' id='filter-expenses'>
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e('Select Date Range:', 'wpaesp'); ?></th>
					<td>
						<?php _e( 'From', 'wpaesp' ); ?> <input type="text" size="10" name="thisdate" id="thisdate" value="" /> 
						<?php _e( 'to', 'wpaesp' ); ?> <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Expense Category', 'wpaesp'); ?></th>
					<td>
						<select name="category" id="category">
							<option value=""> </option>
							<?php $expense_categories = get_terms( 'expense_category', 'hide_empty=0&orderby=name' );
							foreach ( $expense_categories as $category ) { ?>
								<option value="<?php echo $category->slug; ?>" ><?php echo $category->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Expense Status:', 'wpaesp'); ?></th>
					<td>
						<select name="status" id="status">
							<option value=""> </option>
							<?php $statuses = get_terms( 'expense_status', 'hide_empty=0&orderby=name' );
							foreach ( $statuses as $status ) { ?>
								<option value="<?php echo $status->slug; ?>" ><?php echo $status->name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Employee:', 'wpaesp'); ?></th>
					<td>
						<select name="employee">
							<option value=""></option>
							<?php $employees = array_merge( get_users( 'role=employee&orderby=nicename' ), get_users( 'role=administrator&orderby=nicename' ) );
							usort( $employees, 'wpaesp_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<option value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></option>
							<?php } ?>
						</select>
					</td>					
				</tr>
				<tr>
					<th scope="row"><?php _e('Job:', 'wpaesp'); ?></th>
					<td>
						<?php $args = array( 
						    'post_type' => 'job',  
						    'posts_per_page' => -1,  
						    'orderby' => 'name',
						    'order' => 'asc',
						);

						$jobquery = new WP_Query( $args );

						if ( $jobquery->have_posts() ) : ?>
							<select name="job">
								<option value=""></option>
								<?php while ( $jobquery->have_posts() ) : $jobquery->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endwhile; ?>
							</select>
						<?php endif;
						wp_reset_postdata(); ?>
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Filter Expenses', 'wpaesp' ); ?>" />
			</p>
		</form>

		<?php if($_POST) { 

			if( isset( $_POST['thisdate'] ) && isset( $_POST['repeatuntil'] ) && ( $_POST['thisdate'] > $_POST['repeatuntil'] ) ) {
				$message = __( 'The end date must be after the begin date.', 'wpaesp');
				wp_die( $message );
			}

			// our basic args
			$args = array( 
				'post_type' => 'expense',
				'posts_per_page' => -1,
				);

			$criteria = '';

			// add start date and end date to args
			if( isset( $_POST['thisdate'] ) || isset( $_POST['repeatuntil'] ) ) {
				$args['meta_query'] = array();
				if( isset( $_POST['thisdate'] ) && '____-__-__' !== $_POST['thisdate'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['thisdate'],
				         'type' => 'CHAR',
				         'compare' => '>='
						);
					$criteria .= '<li>' . __( 'Begin date: ', 'wpaesp' ) . $_POST['thisdate'] . '</li>';
				}
				
				if( isset( $_POST['repeatuntil'] ) && '____-__-__' !== $_POST['repeatuntil'] ) {
					$args['meta_query'][] = array(
						'key' => '_wpaesm_date',
				         'value' => $_POST['repeatuntil'],
				         'type' => 'CHAR',
				         'compare' => '<='
						);
					$criteria .= '<li>' . __( 'End date: ', 'wpaesp' ) . $_POST['repeatuntil'] . '</li>';
				}
			}

			// add expense category and expense status to args
			if( isset( $_POST['status'] ) || isset( $_POST['category'] ) ) {
				$args['tax_query'] = array(
					'relation' => 'AND',
					);
				if( isset( $_POST['status'] ) && '' !== $_POST['status'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'expense_status',
				        'field' => 'slug',
				        'terms' => $_POST['status'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Status: ', 'wpaesp' ) . $_POST['status'] . '<li>';
				}
				if( isset( $_POST['category'] ) && '' !== $_POST['category'] ) {
					$args['tax_query'][] = array(
						'taxonomy' => 'expense_type',
				        'field' => 'slug',
				        'terms' => $_POST['category'],
				        'operator' => 'IN'
						);
					$criteria .= '<li>' . __( 'Category: ', 'wpaesp' ) . $_POST['type'] . '<li>';
				}
			}

			// add employee and job to args
			if( ( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) || ( isset( $_POST['job'] ) && '' !== $_POST['job'] ) ) {
				$args['connected_type'] = array();
				$args['connected_items'] = array();
				if( isset( $_POST['employee'] ) && '' !== $_POST['employee'] ) {
					$args['connected_type'][] = 'expenses_to_employees';
					$args['connected_items'][] = $_POST['employee'];
					$criteria .= '<li>' . __( 'Employee: ', 'wpaesp' ) . $_POST['employee'] . '<li>';
				}

				// add job to args 
				if( isset( $_POST['job'] ) && '' !== $_POST['job'] ) {
					// this will overwrite employee?
					$args['connected_type'][] = 'expenses_to_jobs';
					$args['connected_items'][] = $_POST['job'];
					$criteria .= '<li>' . __( 'Job: ', 'wpaesp' ) . $_POST['job'] . '<li>';
				}
			}

			echo '<p class="criteria">' . __( 'You searched for: ', 'wpaesp' ) . '<ul>' . $criteria . '</ul></p>' ;

			$filter = new WP_Query( $args );
			
			// The Loop
			if ( $filter->have_posts() ) { ?>
				<table id="filtered-expenses" class="wp-list-table widefat fixed posts">
					<thead>
						<tr>
							<th data-sort='string'><span><?php _e( 'Expense', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Date', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Employee', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Job', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Status', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Category', 'wpaesp' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Amount', 'wpaesp' ); ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $filter->have_posts() ) : $filter->the_post(); 
							$postid = get_the_id();
							global $expense_metabox;
							$meta = $expense_metabox->the_meta(); 
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'expenses_to_employees',
								'connected_items' => $postid,
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employeeid = $user->ID;
								}
							}
							// get job associated with this shift
							$jobs = get_posts( array(
								'connected_type' => 'expenses_to_jobs',
								'connected_items' => $postid,
								'nopaging' => true,
								'suppress_filters' => false
							) );
							if( isset( $jobs ) ) { 
								foreach($jobs as $job) {
									$jobname = $job->post_title;
									$jobid = $job->ID;
								}
							}
							?>
							<tr>
								<td class="title">	
									<?php the_title(); ?><br />
									<a href="<?php echo get_edit_post_link(); ?>"><?php _e( 'Edit', 'wpaesp' ); ?></a> |
									<a href="<?php the_permalink(); ?>"><?php _e( 'View', 'wpaesp' ); ?></a>
								</td>
								<td class="date">
									<?php if( isset( $meta['date'] ) ) {
										echo $meta['date'];
									} ?>
								</td>
								<td class="employee">
									<?php if( isset( $employeeid ) ) { ?>
										<a href="<?php echo get_edit_user_link( $employeeid ); ?>"><?php echo $employee; ?></a>
										<?php unset( $employeeid);
									} ?>
								</td>
								<td class="job">
									<?php if( isset( $jobid ) ) { ?>
										<a href="<?php echo get_edit_post_link( $jobid ); ?>"><?php echo $jobname; ?></a>
									<?php unset( $jobid);
									} ?>
								</td>
								<td class="status">
									<?php $statuslist =  wp_get_post_terms( $postid, 'expense_status' );
									if( isset( $statuslist ) ) {
										foreach($statuslist as $status) {  ?>
											<a href="<?php echo admin_url( 'edit.php?expense_status='.$status->slug.'&post_type=expense' ); ?>"><?php echo $status->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="category">
									<?php $receipt = false;
									$categorylist =  wp_get_post_terms( $postid, 'expense_category' );
									if( isset( $categorylist ) ) {
										foreach($categorylist as $category ) {  
											if( 'receipt' == $category->slug ) {
												$receipt = true;
												// probably also need to check if receipt is the parent
											} ?>
											<a href="<?php echo admin_url( 'edit.php?expense_category='.$category->slug.'&post_type=expense' ); ?>"><?php echo $category->name; ?></a>
										<?php } 
									} ?>
								</td>
								<td class="amount">
									<?php if( isset( $meta['amount'] ) ) {
										if( true == $receipt ) {
											echo "$";
										}
										echo $meta['amount'];
										unset( $receipt );
									} ?>
								</td>
							</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			<?php } else {
				_e( 'No results found.', 'wpaesp' );
			} 
			
			// Reset Post Data
			wp_reset_postdata();

		} ?>


	</div>
<?php } 

?>