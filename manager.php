<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Create Manager user role
 *
 * Managers can edit shifts, jobs, expenses, and users
 *
 */
function wpaesp_create_manager_user_role() {
	// @todo - admin-init check if manager user role exists, and if not, create it - make sure old users get this
	$capabilities = array(
		'read' => true,
		'edit_posts' => false,
		'publish_posts' => false,
		'publish_shifts' => true,
		'edit_shifts' => true,
		'edit_others_shifts' => true,
		'read_private_shifts' => true,
		'edit_shift' => true,
		'delete_shift' => true,
		'read_shift' => true,
	);
	add_role( 'es_manager', 'Manager', $capabilities );
}


add_action( 'p2p_init', 'wpaesp_connect_managers_to_employees' );
/**
 * Create the connection between managers and employees
 */
function wpaesp_connect_managers_to_employees() {
	p2p_register_connection_type( array(
		'name' => 'manager_to_employee',
		'from' => 'user', // manager
		'to' => 'user', // employee
		'admin_column' => 'any',
		'title' => array( 'from' => 'Manager', 'to' => 'Employee(s)' )
	) );
}

add_action( 'show_user_profile', 'wpaesp_user_profile_manager' );
add_action( 'edit_user_profile', 'wpaesp_user_profile_manager' );

/**
 * Display the form to edit manager/employee information on user profile
 *
 * @param $user
 */
function wpaesp_user_profile_manager( $user ) { ?>
	<?php if( is_admin() ) {

		if( wpaesm_check_user_role( 'employee', $user->ID ) ) {
			$managers = get_users( 'orderby=nicename&role=es_manager' );
			if( empty( $managers ) ) { ?>
				<h3><?php _e("Manager", "wpaesp"); ?></h3>
				<p><?php _e( 'Your site does not have any managers.  Create user accounts with the "manager" user role and then you can assign employees to managers.', 'wpaesm' ); ?></p>
			<?php } else {
				$users_manager = new WP_User_Query( array(
					'connected_type' => 'manager_to_employee',
					'connected_items' => $user->ID,
				) );

				if ( ! empty( $users_manager->results ) ) {
					foreach ( $users_manager->results as $manager ) {
						$manager_id = $manager->ID;
					}
				} else {
					$manager_id = 0;
				} ?>


				<h3><?php _e( "Manager", "wpaesp" ); ?></h3>

				<table class="form-table">
					<tr>
						<th><label for="manager"><?php _e( 'Manager', 'wpaesp' ); ?></label></th>
						<td>
							<select name='manager'>
								<option value=''></option>
								<?php foreach( $managers as $manager ) { ?>
									<option value='<?php echo $manager->ID; ?>' <?php selected( $manager->ID, $manager_id ); ?>><?php echo esc_attr( $manager->display_name ); ?></option>
								<?php } ?>

							</select>
						</td>
					</tr>
				</table>
			<?php } ?>


		<?php } elseif( wpaesm_check_user_role( 'es_manager', $user->ID ) ) { ?>
			<h3><?php _e( "Manager's Employees", "wpaesp" ); ?></h3>
			<?php $managers_employees = new WP_User_Query( array(
				'connected_type' => 'manager_to_employee',
				'connected_items' => $user->ID,
				'connected_direction' => 'to',
			) );

			if ( ! empty( $managers_employees->results ) ) { ?>
				<table class="form-table">
					<tr>
						<th><?php printf( __( 'Employees managed by %s', 'wpaesp' ), $user->display_name ); ?></th>
						<td>
							<ul>
								<?php foreach ( $managers_employees->results as $employee ) { ?>
									<li><a href="<?php echo get_edit_user_link(); ?>"><?php echo esc_attr( $employee->display_name ); ?></a></li>
								<?php } ?>
							</ul>
						</td>
					</tr>
				</table>
			<?php } else {
				_e( 'This manager does not have any assigned employees', 'wpaesp' );
			} ?>

			<?php } ?>

	<?php } ?>
<?php }

add_action( 'personal_options_update', 'wpaesp_save_user_profile_manager' );
add_action( 'edit_user_profile_update', 'wpaesp_save_user_profile_manager' );

/**
 * Save manager information to user profile
 *
 * @param $user_id
 *
 * @return bool
 */
function wpaesp_save_user_profile_manager( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	$to = $_POST['manager']; // manager
	$from = $user_id; // employee

	p2p_type( 'manager_to_employee' )->connect( $from, $to, array(
		'date' => current_time('mysql')
	) );

}

